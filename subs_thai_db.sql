/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50626
Source Host           : localhost:3306
Source Database       : subs_thai_test

Target Server Type    : MYSQL
Target Server Version : 50626
File Encoding         : 65001

Date: 2016-10-13 12:01:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for link
-- ----------------------------
DROP TABLE IF EXISTS `link`;
CREATE TABLE `link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keyword` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for mo
-- ----------------------------
DROP TABLE IF EXISTS `mo`;
CREATE TABLE `mo` (
  `mo_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keyword` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shortcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time_request` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telco_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `time_utc` datetime NOT NULL,
  `created_date` date DEFAULT NULL,
  `is_register` tinyint(1) DEFAULT '0',
  `is_send_track` tinyint(1) DEFAULT '0',
  `is_send_track_success` tinyint(1) DEFAULT '0',
  `channel` tinyint(1) default '0',
  PRIMARY KEY (`mo_id`),
  KEY `idx_summary_telco` (`keyword`,`is_register`,`created_date`,`telco_id`),
  KEY `idx_summary` (`keyword`,`is_register`,`created_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `mo_duplicate`;
CREATE TABLE `mo_duplicate` (
  `mo_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keyword` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shortcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time_request` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telco_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `time_utc` datetime NOT NULL,
  `created_date` date DEFAULT NULL,
  `is_register` tinyint(1) DEFAULT '0',
  `channel` tinyint(1) default '0',
  PRIMARY KEY (`mo_id`),
  KEY `idx_summary_telco` (`keyword`,`is_register`,`created_date`,`telco_id`),
  KEY `idx_summary` (`keyword`,`is_register`,`created_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for mo_tmp
-- ----------------------------
DROP TABLE IF EXISTS `mo_tmp`;
CREATE TABLE `mo_tmp` (
  `id` int(11) COLLATE utf8_unicode_ci NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keyword` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shortcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time_request` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
	`time_utc` datetime NOT NULL,
  `telco_id` int(11) DEFAULT NULL,
  `is_register` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for mt
-- ----------------------------
DROP TABLE IF EXISTS `mt`;
CREATE TABLE `mt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `from` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telco_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keyword` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `time_utc` datetime NOT NULL,
  `created_date` date DEFAULT NULL,
  `last_mt_response` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_success` tinyint(1) DEFAULT '0',
  `is_welcome` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_summary` (`keyword`,`is_success`,`is_welcome`,`created_date`,`telco_id`),
  KEY `idx_check_summary` (`type`,`is_success`,`created_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for mt_dtac
-- ----------------------------
DROP TABLE IF EXISTS `mt_dtac`;
CREATE TABLE `mt_dtac` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `from` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telco_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keyword` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `time_utc` datetime NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0',
  `created_date` date DEFAULT NULL,
  `last_mt_response` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_success` tinyint(1) DEFAULT '0',
  `is_welcome` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_summary` (`keyword`,`is_success`,`is_welcome`,`created_date`,`telco_id`),
  KEY `idx_check_summary` (`type`,`is_success`,`created_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for mt_retry
-- ----------------------------
CREATE TABLE `mt_retry` (
  `id` varchar(30) NOT NULL,
  `time_request` datetime NOT NULL,
  `phone` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `shortcode` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `telco_id` smallint(6) DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `time_utc` datetime DEFAULT NULL,
  `keyword` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `retry` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_phone` (`phone`),
  KEY `idx_retry` (`retry`),
  KEY `idx_retry_sum` (`phone`,`retry`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telco_id` int(11) NOT NULL,
  `keyword` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mo_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `time_utc` datetime NOT NULL,
  `last_mt_at` datetime DEFAULT NULL,
  `last_mt_at_utc` datetime DEFAULT NULL,
  `welcome_mt_at` datetime DEFAULT NULL,
  `has_first_mt` bit(1) DEFAULT b'0',
  `last_link_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_mt_response` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `last_mt_response` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_retry_today` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for mt_dn
-- ----------------------------
DROP TABLE IF EXISTS `mt_dn`;
CREATE TABLE `mt_dn` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
  `mt_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mo_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time_request` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shortcode` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telco_id` smallint(6) DEFAULT NULL,
  `country_id` smallint(6) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `time_utc` datetime DEFAULT NULL,
  `is_charge_success` tinyint(1) DEFAULT '0',
  `keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_summary` (`keyword`, `is_charge_success`, `created_date`, `telco_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- ----------------------------
-- Table structure for aff
-- ----------------------------
DROP TABLE IF EXISTS `aff`;
CREATE TABLE `aff` (
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `click_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `telco_id` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `sent` bit(1) DEFAULT b'0',
  `last_updated_at` datetime DEFAULT NULL,
  `response` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pub` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for aff_access
-- ----------------------------
DROP TABLE IF EXISTS `aff_access`;
CREATE TABLE `aff_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `click_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telco_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_date` date DEFAULT NULL,
  `pub` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_summary` (`pub`, `created_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for aff_track
-- ----------------------------
DROP TABLE IF EXISTS `aff_track`;
CREATE TABLE `aff_track` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `click_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `telco_id` int(11) DEFAULT NULL,
  `response` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `pub` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_phone` (`phone`),
  KEY `idx_pub` (`pub`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
