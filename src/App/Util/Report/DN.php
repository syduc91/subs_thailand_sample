<?php

namespace App\Util\Report;

use App\Telco;
use App\Util\DateTime;
use Doctrine\DBAL\Connection;

class DN
{
    /** @var  Connection */
    private $conn;

    public function __construct(Connection $conn)
    {
        $this->conn = $conn;
    }

    public function getChargeSuccess($keyword, $startDate, $endDate, $splitType = DateTime::DIFF_TYPE_DAY)
    {
        $selectDate = 'DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i") as at';
        $groupBy = 'DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i")';
        if (DateTime::DIFF_TYPE_DAY == $splitType) {
            $selectDate = 'DATE_FORMAT(created_date, "%d-%m-%Y") as day';
            $groupBy = 'created_date';
        }

        if (DateTime::DIFF_TYPE_WEEK == $splitType) {
            $selectDate = 'WEEKOFYEAR(created_date) as week';
            $groupBy = 'WEEKOFYEAR(created_date)';
        }

        if (DateTime::DIFF_TYPE_MONTH == $splitType) {
            $selectDate = 'DATE_FORMAT(created_date, "%m-%Y") as month';
            $groupBy = 'MONTH(created_date)';
        }

        $dateCondition = ' (created_date between :startDate AND :endDate) ';
        if (DateTime::DIFF_TYPE_AT == $splitType && $startDate == $endDate) {
            $selectDate = 'DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i") as at';
            $dateCondition = ' time_to_sec(timediff(DATE_FORMAT(:startDate, "%Y-%m-%d %H:%i"), DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i")))/60 = 0 ';
            $groupBy = 'DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i")';
        }

        $sql = 'SELECT '.$selectDate.', count(mt_id) AS count '.
            'FROM mt_dn '.
            'WHERE keyword = :keyword AND is_charge_success = :isSuccess AND '.$dateCondition.
            'GROUP BY '.$groupBy;

        $stmt = $this->conn->prepare($sql);

        $startDate = DateTime::toYearFirstStyle($startDate);
        $endDate = DateTime::toYearFirstStyle($endDate);

        $isSuccess = 1;
        $stmt->bindParam('isSuccess', $isSuccess, \PDO::PARAM_INT);
        $stmt->bindParam('keyword', $keyword);
        $stmt->bindParam('startDate', $startDate);
        if (DateTime::DIFF_TYPE_AT != $splitType) {
            $stmt->bindParam('endDate', $endDate);
        }

        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getChargeSuccessTelco($keyword, $startDate, $endDate, $telco = 'all', $splitType = DateTime::DIFF_TYPE_DAY)
    {
        $selectDate = 'DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i") as at';
        $groupBy = 'DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i")';
        if (DateTime::DIFF_TYPE_DAY == $splitType) {
            $selectDate = 'DATE_FORMAT(created_date, "%d-%m-%Y") as day';
            $groupBy = 'created_date';
        }

        if (DateTime::DIFF_TYPE_WEEK == $splitType) {
            $selectDate = 'WEEKOFYEAR(created_date) as week';
            $groupBy = 'WEEKOFYEAR(created_date)';
        }

        if (DateTime::DIFF_TYPE_MONTH == $splitType) {
            $selectDate = 'DATE_FORMAT(created_date, "%m-%Y") as month';
            $groupBy = 'MONTH(created_date)';
        }

        $dateCondition = ' (created_date between :startDate AND :endDate) ';
        if (DateTime::DIFF_TYPE_AT == $splitType && $startDate == $endDate) {
            $selectDate = 'DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i") as at';
            $dateCondition = ' time_to_sec(timediff(DATE_FORMAT(:startDate, "%Y-%m-%d %H:%i"), DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i")))/60 = 0 ';
            $groupBy = 'DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i")';
        }

        $telcoCondition = '';
        if ('all' != $telco) {
            $telcoCondition = ' AND telco_id = :telco ';
        }

        $sql = 'SELECT '.$selectDate.', telco_id as telco, count(mt_id) AS count '.
            'FROM mt_dn '.
            'WHERE keyword = :keyword AND is_charge_success = :isSuccess AND '.$dateCondition.$telcoCondition.
            'GROUP BY '.$groupBy.', telco_id';

        $stmt = $this->conn->prepare($sql);

        $startDate = DateTime::toYearFirstStyle($startDate);
        $endDate = DateTime::toYearFirstStyle($endDate);

        $isSuccess = 1;
        $stmt->bindParam('isSuccess', $isSuccess, \PDO::PARAM_INT);
        $stmt->bindParam('keyword', $keyword);
        $stmt->bindParam('startDate', $startDate);
        if (DateTime::DIFF_TYPE_AT != $splitType) {
            $stmt->bindParam('endDate', $endDate);
        }
        if ('all' != $telco) {
            $stmt->bindParam('telco', $telco, \PDO::PARAM_INT);
        }
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @param $telcoId
     * @param $status
     *
     * @return bool
     */
    public static function isChargeSuccess($telcoId, $status)
    {
        $status = strtolower(trim($status));
        switch ($telcoId) {
            case Telco::DTAC :
                $isSuccess = static::isChargeSuccessDTAC($status);
                break;
            case Telco::AIS :
                $isSuccess = static::isChargeSuccessAIS($status);
                break;
            case Telco::TRUEMOVE :
                $isSuccess = static::isChargeSuccessTRUEH($status);
                break;
            default :
                $isSuccess = false;
        }

        return $isSuccess;
    }

    /**
     * @param $status
     *
     * @return bool
     */
    public static function isChargeSuccessDTAC($status)
    {
        if ('4' == $status) {
            return true;
        }

        return false;
    }

    /**
     * @param $status
     *
     * @return bool
     */
    public static function isChargeSuccessAIS($status)
    {
        if (false !== strpos($status, 'external:delivrd')) {
            return true;
        }

        return false;
    }

    /**
     * @param $status
     *
     * @return bool
     */
    public static function isChargeSuccessTRUEH($status)
    {
        if (false !== strpos($status, 'sent_delivered: 000') || false !== strpos($status, 'sent_delivered: 999')) {
            return true;
        }

        return false;
    }

} 
