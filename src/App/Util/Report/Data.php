<?php

namespace App\Util\Report;

use App\MO\MO;
use App\MT\MT;
use App\Telco;
use App\Util\DateTime;
use Doctrine\DBAL\Connection;

class Data
{
    /** @var Connection $conn */
    private $conn;

    public function __construct(Connection $conn)
    {
        $this->conn = $conn;
    }

    /**
     * Get MO register
     *
     * @param        $keyword
     * @param        $startDate
     * @param        $endDate
     * @param string $splitType
     *
     * @return array
     */
    public function getMOReg($keyword, $startDate, $endDate, $splitType = DateTime::DIFF_TYPE_DAY)
    {
        return $this->getMO(1, $keyword, $startDate, $endDate, $splitType);
    }

    /**
     * Get MO unregistered
     *
     * @param        $keyword
     * @param        $startDate
     * @param        $endDate
     * @param string $splitType
     *
     * @return array
     */
    public function getMOUnreg($keyword, $startDate, $endDate, $splitType = DateTime::DIFF_TYPE_DAY)
    {
        return $this->getMO(0, $keyword, $startDate, $endDate, $splitType);
    }

    public function getMO($isRegister, $keyword, $startDate, $endDate, $splitType = DateTime::DIFF_TYPE_DAY)
    {
        $selectDate = 'DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i") as at';
        $groupBy = 'DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i")';
        if (DateTime::DIFF_TYPE_DAY == $splitType) {
            $selectDate = 'DATE_FORMAT(created_date, "%d-%m-%Y") as day';
            $groupBy = 'created_date';
        }

        if (DateTime::DIFF_TYPE_WEEK == $splitType) {
            $selectDate = 'WEEKOFYEAR(created_date) as week';
            $groupBy = 'WEEKOFYEAR(created_date)';
        }

        if (DateTime::DIFF_TYPE_MONTH == $splitType) {
            $selectDate = 'DATE_FORMAT(created_date, "%m-%Y") as month';
            $groupBy = 'MONTH(created_date)';
        }

        $dateCondition = ' (created_date between :startDate AND :endDate) ';
        if (DateTime::DIFF_TYPE_AT == $splitType && $startDate == $endDate) {
            $selectDate = 'DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i") as at';
            $dateCondition = ' time_to_sec(timediff(DATE_FORMAT(:startDate, "%Y-%m-%d %H:%i"), DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i")))/60 = 0 ';
            $groupBy = 'DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i")';
        }

        $sql = 'SELECT '.$selectDate.', count(mo_id) AS count '.
            'FROM mo '.
            'WHERE is_register = :isRegister AND keyword = :keyword AND '.$dateCondition.
            'GROUP BY '.$groupBy;

        $stmt = $this->conn->prepare($sql);

        $startDate = DateTime::toYearFirstStyle($startDate);
        $endDate = DateTime::toYearFirstStyle($endDate);

        $stmt->bindParam('isRegister', $isRegister, \PDO::PARAM_INT);
        $stmt->bindParam('keyword', $keyword);
        $stmt->bindParam('startDate', $startDate);
        if (DateTime::DIFF_TYPE_AT != $splitType) {
            $stmt->bindParam('endDate', $endDate);
        }

        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getMOTelco($isRegister, $keyword, $startDate, $endDate, $telco = 'all', $splitType = DateTime::DIFF_TYPE_DAY)
    {
        $selectDate = 'DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i") as at';
        $groupBy = 'DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i")';
        if (DateTime::DIFF_TYPE_DAY == $splitType) {
            $selectDate = 'DATE_FORMAT(created_date, "%d-%m-%Y") as day';
            $groupBy = 'created_date';
        }

        if (DateTime::DIFF_TYPE_WEEK == $splitType) {
            $selectDate = 'WEEKOFYEAR(created_date) as week';
            $groupBy = 'WEEKOFYEAR(created_date)';
        }

        if (DateTime::DIFF_TYPE_MONTH == $splitType) {
            $selectDate = 'DATE_FORMAT(created_date, "%m-%Y") as month';
            $groupBy = 'MONTH(created_date)';
        }

        $dateCondition = ' (created_date between :startDate AND :endDate) ';
        if (DateTime::DIFF_TYPE_AT == $splitType && $startDate == $endDate) {
            $selectDate = 'DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i") as at';
            $dateCondition = ' time_to_sec(timediff(DATE_FORMAT(:startDate, "%Y-%m-%d %H:%i"), DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i")))/60 = 0 ';
            $groupBy = 'DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i")';
        }

        $telcoCondition = '';
        if ('all' != $telco) {
            $telcoCondition = ' AND telco_id = :telco ';
        }

        $sql = 'SELECT '.$selectDate.', telco_id as telco, count(mo_id) AS count '.
            'FROM mo '.
            'WHERE keyword = :keyword AND is_register = :isRegister AND '.$dateCondition.$telcoCondition.
            'GROUP BY '.$groupBy.', telco_id';

        $stmt = $this->conn->prepare($sql);

        $startDate = DateTime::toYearFirstStyle($startDate);
        $endDate = DateTime::toYearFirstStyle($endDate);

        $stmt->bindParam('isRegister', $isRegister, \PDO::PARAM_INT);
        $stmt->bindParam('keyword', $keyword);
        $stmt->bindParam('startDate', $startDate);
        if (DateTime::DIFF_TYPE_AT != $splitType) {
            $stmt->bindParam('endDate', $endDate);
        }
        if ('all' != $telco) {
            $stmt->bindParam('telco', $telco, \PDO::PARAM_INT);
        }
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Get MO register filter by telco
     *
     * @param        $keyword
     * @param        $startDate
     * @param        $endDate
     * @param string $telco
     * @param string $splitType
     *
     * @return array
     */
    public function getMORegTelco($keyword, $startDate, $endDate, $telco = 'all', $splitType = DateTime::DIFF_TYPE_DAY)
    {
        return $this->getMOTelco(1, $keyword, $startDate, $endDate, $telco, $splitType);
    }

    /**
     * Get MO unregistered filter by telco
     *
     * @param        $keyword
     * @param        $startDate
     * @param        $endDate
     * @param string $telco
     * @param string $splitType
     *
     * @return array
     */
    public function getMOUnregTelco($keyword, $startDate, $endDate, $telco = 'all', $splitType = DateTime::DIFF_TYPE_DAY)
    {
        return $this->getMOTelco(0, $keyword, $startDate, $endDate, $telco, $splitType);
    }

    /**
     * Get User
     *
     * @param        $keyword
     * @param        $startDate
     * @param        $endDate
     * @param string $splitType
     *
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getUser($keyword, $startDate, $endDate, $splitType = DateTime::DIFF_TYPE_DAY)
    {
        $selectDate = 'created_at';
        $groupBy = 'DATE(DATE_ADD(created_at, INTERVAL 1 HOUR))';
        if (DateTime::DIFF_TYPE_DAY == $splitType) {
            $selectDate = 'DATE_FORMAT(created_date, "%d-%m-%Y") as day';
            $groupBy = 'created_date';
        }

        if (DateTime::DIFF_TYPE_WEEK == $splitType) {
            $selectDate = 'WEEKOFYEAR(created_date) as week';
            $groupBy = 'WEEKOFYEAR(created_date)';
        }

        if (DateTime::DIFF_TYPE_MONTH == $splitType) {
            $selectDate = 'DATE_FORMAT(created_date, "%m-%Y") as month';
            $groupBy = 'MONTH(created_date)';
        }

        $sql = 'SELECT '.$selectDate.', count(mo_id) AS count '.
            'FROM mo '.
            'WHERE text = :text AND (created_date between :startDate AND :endDate) '.
            'GROUP BY '.$groupBy;
        $stmt = $this->conn->prepare($sql);

        $startDate = DateTime::toYearFirstStyle($startDate);
        $endDate = DateTime::toYearFirstStyle($endDate);
        $text = $keyword;

        $stmt->bindParam('text', $text);
        $stmt->bindParam('startDate', $startDate);
        $stmt->bindParam('endDate', $endDate);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Get User filter by telco
     *
     * @param        $keyword
     * @param        $startDate
     * @param        $endDate
     * @param string $telco
     * @param string $splitType
     *
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getUserTelco($keyword, $startDate, $endDate, $telco = 'all', $splitType = DateTime::DIFF_TYPE_DAY)
    {
        $selectDate = 'created_at';
        $groupBy = 'DATE(DATE_ADD(created_at, INTERVAL 1 HOUR))';
        if (DateTime::DIFF_TYPE_DAY == $splitType) {
            $selectDate = 'DATE_FORMAT(created_date, "%d-%m-%Y") as day';
            $groupBy = 'created_date';
        }

        if (DateTime::DIFF_TYPE_WEEK == $splitType) {
            $selectDate = 'WEEKOFYEAR(created_date) as week';
            $groupBy = 'WEEKOFYEAR(created_date)';
        }

        if (DateTime::DIFF_TYPE_MONTH == $splitType) {
            $selectDate = 'DATE_FORMAT(created_date, "%m-%Y") as month';
            $groupBy = 'MONTH(created_date)';
        }

        $telcoCondition = '';
        if ('all' != $telco) {
            $telcoCondition = ' AND telco_id = :telco ';
        }
        $sql = 'SELECT '.$selectDate.', telco_id as telco, count(mo_id) AS count '.
            'FROM mo '.
            'WHERE text = :text AND (created_date between :startDate AND :endDate) '.
            $telcoCondition.
            'GROUP BY '.$groupBy.', telco_id';

        $stmt = $this->conn->prepare($sql);

        $startDate = DateTime::toYearFirstStyle($startDate);
        $endDate = DateTime::toYearFirstStyle($endDate);
        $text = $keyword;

        $stmt->bindParam('text', $text);
        $stmt->bindParam('startDate', $startDate);
        $stmt->bindParam('endDate', $endDate);
        if ('all' != $telco) {
            $stmt->bindParam('telco', $telco);
        }
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getMT($isSuccess, $keyword, $startDate, $endDate, $splitType = DateTime::DIFF_TYPE_DAY)
    {
        $selectDate = 'DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i") as at';
        $groupBy = 'DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i")';

        if (DateTime::DIFF_TYPE_DAY == $splitType) {
            $selectDate = 'DATE_FORMAT(created_date, "%d-%m-%Y") as day';
            $groupBy = 'created_date';
        }

        if (DateTime::DIFF_TYPE_WEEK == $splitType) {
            $selectDate = 'WEEKOFYEAR(created_date) as week';
            $groupBy = 'WEEKOFYEAR(created_date)';
        }

        if (DateTime::DIFF_TYPE_MONTH == $splitType) {
            $selectDate = 'DATE_FORMAT(created_date, "%m-%Y") as month';
            $groupBy = 'MONTH(created_date)';
        }

        $dateCondition = ' (created_date between :startDate AND :endDate) ';
        if (DateTime::DIFF_TYPE_AT == $splitType && $startDate == $endDate) {
            $selectDate = 'DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i") as at';
            $dateCondition = ' time_to_sec(timediff(DATE_FORMAT(:startDate, "%Y-%m-%d %H:%i"), DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i")))/60 = 0 ';
            $groupBy = 'DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i")';
        }

        $sqlMT = 'SELECT '.$selectDate.', count(id) AS count '.
            'FROM mt '.
            'WHERE keyword = :keyword AND is_success = :isSuccess AND is_welcome = 0 AND '.$dateCondition.
            'GROUP BY '.$groupBy;

        $sqlMTDTAC = 'SELECT '.$selectDate.', sum(`count`) AS count '.
            'FROM mt_dtac '.
            'WHERE keyword = :keyword AND is_success = :isSuccess AND is_welcome = 0 AND '.$dateCondition.
            'GROUP BY '.$groupBy;

        $sql = $sqlMT.' UNION '.$sqlMTDTAC;

        $stmt = $this->conn->prepare($sql);
        $startDate = DateTime::toYearFirstStyle($startDate);
        $endDate = DateTime::toYearFirstStyle($endDate);

        $stmt->bindParam('isSuccess', $isSuccess, \PDO::PARAM_INT);
        $stmt->bindParam('keyword', $keyword);
        $stmt->bindParam('startDate', $startDate);
        if (DateTime::DIFF_TYPE_AT != $splitType) {
            $stmt->bindParam('endDate', $endDate);
        }

        $stmt->execute();

        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        //Merge two data of table `mt` & `mt_dtac`
        $result = [];
        foreach ($data as $index => $item) {
            $result[$item[$splitType]][$splitType] = $item[$splitType];

            if (!isset($result[$item[$splitType]]['count'])) {
                $result[$item[$splitType]]['count'] = $item['count'];
            } else {
                $result[$item[$splitType]]['count'] += $item['count'];
            }
        }

        return array_values($result);
    }

    /**
     * Get MT success
     *
     * @param        $keyword
     * @param        $startDate
     * @param        $endDate
     * @param string $splitType
     *
     * @return array
     */
    public function getMTSuccess($keyword, $startDate, $endDate, $splitType = DateTime::DIFF_TYPE_DAY)
    {
        return $this->getMT(1, $keyword, $startDate, $endDate, $splitType);
    }

    /**
     * Get MT fail
     *
     * @param        $keyword
     * @param        $startDate
     * @param        $endDate
     * @param string $splitType
     *
     * @return array
     */
    public function getMTFail($keyword, $startDate, $endDate, $splitType = DateTime::DIFF_TYPE_DAY)
    {
        return $this->getMT(0, $keyword, $startDate, $endDate, $splitType);
    }

    /**
     * Get MT filter by telco
     *
     * @param        $keyword
     * @param        $startDate
     * @param        $endDate
     * @param        $isSuccess
     * @param string $telco
     * @param string $splitType
     *
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getMTTelco($isSuccess, $keyword, $startDate, $endDate, $telco = 'all', $splitType = DateTime::DIFF_TYPE_DAY)
    {
        $selectDate = 'DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i") as at';
        $groupBy = 'DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i")';

        if (DateTime::DIFF_TYPE_DAY == $splitType) {
            $selectDate = 'DATE_FORMAT(created_date, "%d-%m-%Y") as day';
            $groupBy = 'created_date';
        }

        if (DateTime::DIFF_TYPE_WEEK == $splitType) {
            $selectDate = 'WEEKOFYEAR(created_date) as week';
            $groupBy = 'WEEKOFYEAR(created_date)';
        }

        if (DateTime::DIFF_TYPE_MONTH == $splitType) {
            $selectDate = 'DATE_FORMAT(created_date, "%m-%Y") as month';
            $groupBy = 'MONTH(created_date)';
        }

        $dateCondition = ' (created_date between :startDate AND :endDate) ';
        if (DateTime::DIFF_TYPE_AT == $splitType && $startDate == $endDate) {
            $selectDate = 'DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i") as at';
            $dateCondition = ' time_to_sec(timediff(DATE_FORMAT(:startDate, "%Y-%m-%d %H:%i"), DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i")))/60 = 0 ';
            $groupBy = 'DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i")';
        }

        $telcoCondition = '';
        if ('all' != $telco) {
            $telcoCondition = ' AND telco_id = :telco ';
        }

        $sql = 'SELECT '.$selectDate.', telco_id as telco, count(id) AS count '.
            'FROM mt '.
            'WHERE keyword = :keyword AND is_success = :isSuccess AND is_welcome = 0 AND '.$dateCondition.$telcoCondition.
            'GROUP BY '.$groupBy.', telco_id';
        $stmt = $this->conn->prepare($sql);

        $startDate = DateTime::toYearFirstStyle($startDate);
        $endDate = DateTime::toYearFirstStyle($endDate);

        $stmt->bindParam('isSuccess', $isSuccess, \PDO::PARAM_INT);
        $stmt->bindParam('keyword', $keyword);
        $stmt->bindParam('startDate', $startDate);
        if (DateTime::DIFF_TYPE_AT != $splitType) {
            $stmt->bindParam('endDate', $endDate);
        }
        if ('all' != $telco) {
            $stmt->bindParam('telco', $telco);
        }

        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Get MT success filter by telco
     *
     * @param        $keyword
     * @param        $startDate
     * @param        $endDate
     * @param string $telco
     * @param string $splitType
     *
     * @return array
     */
    public function getMTTelcoSuccess($keyword, $startDate, $endDate, $telco = 'all', $splitType = DateTime::DIFF_TYPE_DAY)
    {
        if (Telco::DTAC == $telco) {
            return $this->getMTTelcoDTAC(1, $keyword, $startDate, $endDate, $splitType);
        }

        if ('all' == $telco) {
            $mtTelcoDTAC = $this->getMTTelcoDTAC(1, $keyword, $startDate, $endDate, $splitType);

            return array_merge_recursive($mtTelcoDTAC, $this->getMTTelco(1, $keyword, $startDate, $endDate, $telco, $splitType));
        }

        return $this->getMTTelco(1, $keyword, $startDate, $endDate, $telco, $splitType);
    }

    /**
     * Get MT fail filter by telco
     *
     * @param        $keyword
     * @param        $startDate
     * @param        $endDate
     * @param string $telco
     * @param string $splitType
     *
     * @return array
     */
    public function getMTTelcoFail($keyword, $startDate, $endDate, $telco = 'all', $splitType = DateTime::DIFF_TYPE_DAY)
    {
        $mtTelcoDTAC = $this->getMTTelcoDTAC(0, $keyword, $startDate, $endDate, $splitType);
        if (Telco::DTAC == $telco) {
            return $mtTelcoDTAC;
        }

        if ('all' == $telco) {
            return array_merge_recursive($mtTelcoDTAC, $this->getMTTelco(0, $keyword, $startDate, $endDate, $telco, $splitType));
        }

        return $this->getMTTelco(0, $keyword, $startDate, $endDate, $telco, $splitType);
    }

    /**
     * Get MT Telco DTAC
     *
     * @param        $isSuccess
     * @param        $keyword
     * @param        $startDate
     * @param        $endDate
     * @param string $splitType
     *
     * @throws \Doctrine\DBAL\DBALException
     * @return array
     */
    public function getMTTelcoDTAC($isSuccess, $keyword, $startDate, $endDate, $splitType = DateTime::DIFF_TYPE_DAY)
    {
        $selectDate = 'DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i") as at';
        $groupBy = 'DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i")';

        if (DateTime::DIFF_TYPE_DAY == $splitType) {
            $selectDate = 'DATE_FORMAT(created_date, "%d-%m-%Y") as day';
            $groupBy = 'created_date';
        }

        if (DateTime::DIFF_TYPE_WEEK == $splitType) {
            $selectDate = 'WEEKOFYEAR(created_date) as week';
            $groupBy = 'WEEKOFYEAR(created_date)';
        }

        if (DateTime::DIFF_TYPE_MONTH == $splitType) {
            $selectDate = 'DATE_FORMAT(created_date, "%m-%Y") as month';
            $groupBy = 'MONTH(created_date)';
        }

        $dateCondition = ' (created_date between :startDate AND :endDate) ';
        if (DateTime::DIFF_TYPE_AT == $splitType && $startDate == $endDate) {
            $selectDate = 'DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i") as at';
            $dateCondition = ' time_to_sec(timediff(DATE_FORMAT(:startDate, "%Y-%m-%d %H:%i"), DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i")))/60 = 0 ';
            $groupBy = 'DATE_FORMAT(DATE_ADD(created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i")';
        }

        $sql = 'SELECT '.$selectDate.', telco_id as telco, sum(`count`) AS count '.
            'FROM mt_dtac '.
            'WHERE keyword = :keyword AND is_success = :isSuccess AND is_welcome = 0 AND'.$dateCondition.' AND telco_id = :telcoDTAC '.
            'GROUP BY '.$groupBy.', telco_id';

        $stmt = $this->conn->prepare($sql);

        $startDate = DateTime::toYearFirstStyle($startDate);
        $endDate = DateTime::toYearFirstStyle($endDate);
        $telcoDTAC = Telco::DTAC;

        $stmt->bindParam('isSuccess', $isSuccess, \PDO::PARAM_INT);
        $stmt->bindParam('keyword', $keyword);
        $stmt->bindParam('startDate', $startDate);
        if (DateTime::DIFF_TYPE_AT != $splitType) {
            $stmt->bindParam('endDate', $endDate);
        }
        $stmt->bindParam('telcoDTAC', $telcoDTAC);

        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    //Report for track
    public function getMOTrackRegister($keyword, $startDate, $endDate, $pub, $splitType = DateTime::DIFF_TYPE_DAY)
    {
        return $this->getMOTrack(1, $keyword, $startDate, $endDate, $pub, $splitType);
    }

    public function getMOTrackUnregistered($keyword, $startDate, $endDate, $pub, $splitType = DateTime::DIFF_TYPE_DAY)
    {
        return $this->getMOTrack(0, $keyword, $startDate, $endDate, $pub, $splitType);
    }

    public function getMOTrack($isRegister, $keyword, $startDate, $endDate, $pub, $splitType = DateTime::DIFF_TYPE_DAY)
    {
        $selectDate = 'DATE_FORMAT(DATE_ADD(mo.created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i") as at';
        $groupBy = 'DATE_FORMAT(DATE_ADD(mo.created_at, INTERVAL 1 HOUR), "%Y-%m-%d %H:%i")';
        if (DateTime::DIFF_TYPE_DAY == $splitType) {
            $selectDate = 'DATE_FORMAT(mo.created_date, "%d-%m-%Y") as day';
            $groupBy = 'mo.created_date';
        }

        if (DateTime::DIFF_TYPE_WEEK == $splitType) {
            $selectDate = 'WEEKOFYEAR(mo.created_date) as week';
            $groupBy = 'WEEKOFYEAR(mo.created_date)';
        }

        if (DateTime::DIFF_TYPE_MONTH == $splitType) {
            $selectDate = 'DATE_FORMAT(mo.created_date, "%m-%Y") as month';
            $groupBy = 'MONTH(mo.created_date)';
        }

        $dateCondition = ' (mo.created_date between :startDate AND :endDate) ';

        if (DateTime::DIFF_TYPE_AT == $splitType && $startDate == $endDate) {
            $selectDate = 'DATE_FORMAT(mo.created_at, "%Y-%m-%d %H:%i") as at';
            $dateCondition = ' time_to_sec(timediff(DATE_FORMAT(:startDate, "%Y-%m-%d %H:%i"), DATE_FORMAT(mo.created_at, "%Y-%m-%d %H:%i")))/60 = 0 ';
            $groupBy = 'DATE_FORMAT(mo.created_at, "%Y-%m-%d %H:%i")';
        }

        $sql = 'SELECT '.$selectDate.', count(distinct mo.phone) AS count '.
            'FROM mo INNER JOIN aff_track ON mo.phone = aff_track.phone '.
            'WHERE mo.is_register = :isRegister AND mo.keyword = :keyword AND aff_track.pub = :pub AND '.$dateCondition.
            'GROUP BY '.$groupBy;

        $stmt = $this->conn->prepare($sql);

        $startDate = DateTime::toYearFirstStyle($startDate);
        $endDate = DateTime::toYearFirstStyle($endDate);

        $stmt->bindParam('isRegister', $isRegister, \PDO::PARAM_INT);
        $stmt->bindParam('keyword', $keyword);
        $stmt->bindParam('startDate', $startDate);
        $stmt->bindParam('pub', $pub);
        if (DateTime::DIFF_TYPE_AT != $splitType) {
            $stmt->bindParam('endDate', $endDate);
        }

        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getMOTrackTelcoRegister($keyword, $startDate, $endDate, $pub, $telco = 'all', $splitType = DateTime::DIFF_TYPE_DAY)
    {
        return $this->getMOTrackTelco(1, $keyword, $startDate, $endDate, $pub, $telco, $splitType);
    }

    public function getMOTrackTelcoUnregistered($keyword, $startDate, $endDate, $pub, $telco = 'all', $splitType = DateTime::DIFF_TYPE_DAY)
    {
        return $this->getMOTrackTelco(0, $keyword, $startDate, $endDate, $pub, $telco, $splitType);
    }

    public function getMOTrackTelco($isRegister, $keyword, $startDate, $endDate, $pub, $telco = 'all', $splitType = DateTime::DIFF_TYPE_DAY)
    {
        if (DateTime::DIFF_TYPE_DAY == $splitType) {
            $selectDate = 'DATE_FORMAT(mo.created_date, "%d-%m-%Y") as day';
            $groupBy = 'mo.created_date';
        }

        if (DateTime::DIFF_TYPE_WEEK == $splitType) {
            $selectDate = 'WEEKOFYEAR(mo.created_date) as week';
            $groupBy = 'WEEKOFYEAR(mo.created_date)';
        }

        if (DateTime::DIFF_TYPE_MONTH == $splitType) {
            $selectDate = 'DATE_FORMAT(mo.created_date, "%m-%Y") as month';
            $groupBy = 'MONTH(mo.created_date)';
        }

        $dateCondition = ' (mo.created_date between :startDate AND :endDate) ';

        if (DateTime::DIFF_TYPE_AT == $splitType && $startDate == $endDate) {
            $selectDate = 'DATE_FORMAT(mo.created_at, "%Y-%m-%d %H:%i") as at';
            $dateCondition = ' time_to_sec(timediff(DATE_FORMAT(:startDate, "%Y-%m-%d %H:%i"), DATE_FORMAT(mo.created_at, "%Y-%m-%d %H:%i")))/60 = 0 ';
            $groupBy = 'DATE_FORMAT(mo.created_at, "%Y-%m-%d %H:%i")';
        }

        $telcoCondition = '';
        if ('all' != $telco) {
            $telcoCondition = ' AND mo.telco_id = :telco ';
        }

        $sql = 'SELECT '.$selectDate.', mo.telco_id as telco, count(distinct mo.phone) AS count '.
            'FROM mo INNER JOIN aff_track ON mo.phone = aff_track.phone '.
            'WHERE mo.keyword = :keyword AND mo.is_register = :isRegister AND '.$dateCondition.$telcoCondition.' AND aff_track.pub = :pub '.
            'GROUP BY '.$groupBy.', mo.telco_id';

        $stmt = $this->conn->prepare($sql);

        $startDate = DateTime::toYearFirstStyle($startDate);
        $endDate = DateTime::toYearFirstStyle($endDate);

        $stmt->bindParam('isRegister', $isRegister, \PDO::PARAM_INT);
        $stmt->bindParam('keyword', $keyword);
        $stmt->bindParam('startDate', $startDate);
        $stmt->bindParam('pub', $pub);
        if (DateTime::DIFF_TYPE_AT != $splitType) {
            $stmt->bindParam('endDate', $endDate);
        }
        if ('all' != $telco) {
            $stmt->bindParam('telco', $telco);
        }
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getClickId($startDate, $endDate, $pub, $splitType = DateTime::DIFF_TYPE_DAY)
    {
        $selectDate = 'DATE_FORMAT(created_at, "%Y-%m-%d %H:%i") as at';
        $groupBy = 'DATE_FORMAT(created_at, "%Y-%m-%d %H:%i")';
        if (DateTime::DIFF_TYPE_DAY == $splitType) {
            $selectDate = 'DATE_FORMAT(created_date, "%d-%m-%Y") as day';
            $groupBy = 'created_date';
        }

        if (DateTime::DIFF_TYPE_WEEK == $splitType) {
            $selectDate = 'WEEKOFYEAR(created_date) as week';
            $groupBy = 'WEEKOFYEAR(created_date)';
        }

        if (DateTime::DIFF_TYPE_MONTH == $splitType) {
            $selectDate = 'DATE_FORMAT(created_date, "%m-%Y") as month';
            $groupBy = 'MONTH(created_date)';
        }

        $dateCondition = ' (created_date between :startDate AND :endDate) ';
        if (DateTime::DIFF_TYPE_AT == $splitType && $startDate == $endDate) {
            $selectDate = 'DATE_FORMAT(created_at, "%Y-%m-%d %H:%i") as at';
            $dateCondition = ' time_to_sec(timediff(DATE_FORMAT(:startDate, "%Y-%m-%d %H:%i"), DATE_FORMAT(created_at, "%Y-%m-%d %H:%i")))/60 = 0 ';
            $groupBy = 'DATE_FORMAT(created_at, "%Y-%m-%d %H:%i")';
        }

        $sql = 'SELECT '.$selectDate.', count(click_id) AS count '.
            'FROM aff_access '.
            'WHERE pub = :pub AND '.$dateCondition.
            'GROUP BY '.$groupBy;

        $stmt = $this->conn->prepare($sql);

        $startDate = DateTime::toYearFirstStyle($startDate);
        $endDate = DateTime::toYearFirstStyle($endDate);

        $stmt->bindParam('startDate', $startDate);
        $stmt->bindParam('pub', $pub);
        if (DateTime::DIFF_TYPE_AT != $splitType) {
            $stmt->bindParam('endDate', $endDate);
        }

        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    //End report for track

    /**
     * Get value merged of ref data and raw data
     *
     * ex: ref: ['1-9-2016', '2-9-2016', '3-9-2016']
     *      data: ['1-9-2016' => 5, '3-9-2016' => 2]
     * => result: ['1-9-2016' => 5, '2-9-2016' => 0, '3-9-2016' => 2]
     *
     * @param $ref
     * @param $data
     *
     * @return array
     */
    public static function merge($ref, $data)
    {
        $values = [];
        foreach ($ref as $label) {
            $values[$label] = 0;
        }

        return self::arrayMerge($values, $data);
    }

    /**
     * Format data summary
     *
     * Ex: - ref: ['1-9-2016', '2-9-2016', '3-9-2016']
     *     - data: [["day" => "16-09-2016","count" => "1"],["day":"17-09-2016","count" => "3"]]
     *     - unit: day
     * => result: [0,0,0,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0]
     *
     * @param $ref
     * @param $data
     * @param $unit
     *
     * @return array
     */
    public static function formatDataSummary($ref, $data, $unit)
    {
        $combined = array_combine(array_column($data, $unit), array_column($data, 'count'));

        $values = [];
        foreach ($ref as $label) {

            //fix for $unit = 'week' -> ok
            if (2 === strlen($label)) {
                $value = isset($combined[intval($label)]) ? $combined[intval($label)] : 0;
            } else {
                $value = isset($combined[$label]) ? $combined[$label] : 0;
            }
            $values[] = $value;
        }

        return $values;
    }

    /**
     * Format data filtered by telco
     *
     * Ex:  - ref: ['15-9-2016', '16-9-2016', '17-9-2016', ...]
     *      - data: [["day" => "16-09-2016","telco" => "4","count" => "1"], ["day" => "17-09-2016","telco" => "1","count" => "2"],["day":"17-09-2016","telco" => "3","count" => "1"]]
     *      - unit: day
     * => result: ["XL" => [0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0],"Telkomsel" => [0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0],"IM3" => [0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0]]
     *
     * @param $ref
     * @param $data
     * @param $unit
     *
     * @return array
     */
    public static function formatDataTelco($ref, $data, $unit)
    {
        $telcoData = [];

        foreach ($data as $item) {
            //Fix for unit = week -> ok
            if (strlen($item[$unit]) < 2) {
                $item[$unit] = '0'.$item[$unit];
            }

            if (!isset($telcoData[Telco::$TELCOS[$item['telco']]])) {
                $value = [$item[$unit] => $item['count']];
            } else {
                $value = self::arrayMerge($telcoData[Telco::$TELCOS[$item['telco']]], [$item[$unit] => $item['count']]);
            }
            $telcoData[Telco::$TELCOS[$item['telco']]] = self::merge($ref, $value);
        }

        foreach ($telcoData as $id => $values) {
            $telcoData[$id] = array_values($values);
        }

        return $telcoData;
    }

    private static function arrayMerge($first, $second)
    {
        $result = [];
        foreach ($first as $key => $value) {
            $result[$key] = $value;
        }

        foreach ($second as $key => $value) {
            $result[$key] = $value;
        }

        return $result;
    }
}
