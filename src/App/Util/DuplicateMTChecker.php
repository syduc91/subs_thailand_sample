<?php

namespace App\Util;

class DuplicateMTChecker
{
    const TYPE_WELCOME = 0;
    const TYPE_FIRST = 1;
    const TYPE_DAILY = 2;
    const TYPE_RETRY = 3;

    const KEY_PREFIX = 'mt_check_';

    /** @var  \Redis */
    private $redis;

    public function __construct(\Redis $redis)
    {
        $this->redis = $redis;
    }

    /**
     * Check mt is sent
     *
     * @param $keyword
     * @param $phone
     * @param $price
     * @param $type | values: TYPE_WELCOME || TYPE_FIRST || TYPE_DAILY || TYPE_RETRY
     *
     * @return bool
     */
    public function isSentMT($keyword, $phone, $price, $type)
    {
        return !$this->redis->hSetNx($this->getParentKey($keyword), $this->getChildKey($phone, $price, $type), 1);
    }

    private function getParentKey($keyword)
    {
        return self::KEY_PREFIX.strtolower($keyword).'_'.date('dmY');
    }

    private function getChildKey($phone, $price, $type)
    {
        return $phone.'_'.$type.'_'.$price;
    }
}