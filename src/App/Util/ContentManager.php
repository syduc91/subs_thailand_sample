<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 11/10/2016
 * Time: 10:32 AM
 */

namespace App\Util;

use Doctrine\DBAL\Connection;

class ContentManager
{
    private $conn;

    public function __construct(Connection $conn)
    {
        $this->conn = $conn;
    }

    public function getNextLink($keyword, $currentLinkId)
    {
        return $this->getLinkInfo($keyword, $currentLinkId, '>');
    }

    public function getPreviousLink($keyword, $currentLinkId)
    {
        return $this->getLinkInfo($keyword, $currentLinkId, '<');
    }

    public function getLinkInfo($keyword, $currentLinkId, $compareCharacter = '=')
    {
        //Check mysql timeout
        if (false === $this->conn->ping()) {
            $this->conn->close();
            $this->conn->connect();
        }

        if (null == $currentLinkId) {
            $currentLinkId = 0;
        }

        $sql = 'SELECT * FROM link WHERE keyword = :keyword AND id '.$compareCharacter.' :currentLinkId ORDER BY id ASC LIMIT 1';
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam('keyword', $keyword);
        $stmt->bindParam('currentLinkId', $currentLinkId);
        $stmt->execute();

        $linkInfo = $stmt->fetch(\PDO::FETCH_ASSOC);

        $result = [
            'id' => null,
            'link' => null
        ];
        if (empty($linkInfo)) {
            //get first link
            $sql = 'SELECT * FROM link WHERE keyword = :keyword ORDER BY id ASC LIMIT 1';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam('keyword', $keyword);
            $stmt->execute();

            $check = $stmt->fetch(\PDO::FETCH_ASSOC);
            if (!empty($check)) {
                $result['id'] = $check['id'];
                $result['link'] = $check['link'];
            }
        } else {
            $result = [
                'id' => $linkInfo['id'],
                'link' => $linkInfo['link']
            ];
        }

        //Close connection
        if (false !== $this->conn->ping()) {
            $this->conn->close();
        }

        return $result;
    }
} 
