<?php

namespace App\Util;

class Common
{
    /**
     * Create query string with value url-encoded
     *
     * @param array $params
     *
     * @return string
     */
    public static function buildQueryString(array $params)
    {
        return http_build_query($params, null, '&', PHP_QUERY_RFC3986);
    }

    /**
     * Change mod path recursive
     *
     * @param $path
     * @param $fileMode
     *
     * @return bool
     */
    public static function chmodRecursive($path, $fileMode)
    {
        if (!is_dir($path)) {
            return chmod($path, $fileMode);
        }

        $dh = opendir($path);
        while (($file = readdir($dh)) !== false) {
            if ($file != '.' && $file != '..') {
                $fullPath = $path.'/'.$file;
                if (is_link($fullPath) || !self::chmodRecursive($fullPath, $fileMode) || (!is_dir($fullPath) && !chmod($fullPath, $fileMode))) {
                    return false;
                }
            }
        }
        closedir($dh);
        if (chmod($path, $fileMode)) {
            return true;
        }

        return false;
    }

    /**
     * Change owner path recursive
     *
     * @param $path
     * @param $owner
     *
     * @return bool
     */
    public static function chownRecursive($path, $owner)
    {
        if (!is_dir($path)) {
            return chown($path, $owner);
        }

        $dh = opendir($path);
        while (($file = readdir($dh)) !== false) {
            if ($file != '.' && $file != '..') {
                $fullPath = $path.'/'.$file;
                if (is_link($fullPath) || (!is_dir($fullPath) && !chown($fullPath, $owner)) || !self::chownRecursive($fullPath, $owner)) {
                    return false;
                }
            }
        }
        closedir($dh);
        if (chown($path, $owner)) {
            return true;
        }

        return false;
    }

    /**
     * Change group path recursive
     *
     * @param $path
     * @param $group
     *
     * @return bool
     */
    public static function chgrpRecursive($path, $group)
    {
        if (!is_dir($path)) {
            return chgrp($path, $group);
        }

        $dh = opendir($path);
        while (($file = readdir($dh)) !== false) {
            if ($file != '.' && $file != '..') {
                $fullPath = $path.'/'.$file;
                if (is_link($fullPath) || (!is_dir($fullPath) && !chgrp($fullPath, $group)) || !self::chgrpRecursive($fullPath, $group)) {
                    return false;
                }
            }
        }

        closedir($dh);
        if (chgrp($path, $group)) {
            return true;
        }

        return false;
    }

    /**
     * Reads entire file into an array
     *
     * @param $file
     *
     * @return array
     */
    public static function file2Array($file)
    {
        if (!is_file($file)) {
            return [];
        }

        return array_values(array_filter(file($file, FILE_IGNORE_NEW_LINES)));
    }

    /**
     * Paging an array
     *
     * @param array $input
     * @param       $pageNum
     * @param       $perPage
     *
     * @return array
     */
    public static function pagingArray(array $input, $pageNum, $perPage)
    {
        $start = ($pageNum - 1) * $perPage;
        $end = $start + $perPage;
        $count = count($input);

        // Conditionally return results
        if ($start < 0 || $count <= $start) {
            // Page is out of range
            return array();
        } elseif ($count <= $end) {
            // Partially-filled page
            return array_slice($input, $start);
        } else {
            // Full page
            return array_slice($input, $start, $end - $start);
        }
    }
} 
