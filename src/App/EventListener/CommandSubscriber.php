<?php

namespace App\EventListener;

use App\AppEvents;
use App\MT\MT;
use App\Util\DateTime;
use Doctrine\DBAL\Connection;
use Psr\Log\LoggerInterface;
use Skyads\Logger\Logger;
use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\Console\Event\ConsoleExceptionEvent;
use Symfony\Component\Console\Event\ConsoleTerminateEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Filesystem\LockHandler;
use Symfony\Component\HttpKernel\KernelEvents;

class CommandSubscriber implements EventSubscriberInterface
{
    const DIFF_LIMIT = 1000;
    private $logger;
    private $conn;

    public function __construct(Logger $logger, Connection $conn)
    {
        $this->logger = $logger;
        $this->conn = $conn;
    }

    public function onConsoleException(ConsoleExceptionEvent $event)
    {
        $this->unlockCommand($event->getCommand()->getName());
        $this->logger->noticeSlack('Command bị lỗi exception!', [
            'command' => addcslashes($event->getCommand()->getName(), ':'),
            'message' => $event->getException()->getMessage(),
            'code' => $event->getExitCode()
        ]);
    }

    /**
     * Execute when command is terminated
     *
     * @param ConsoleTerminateEvent $event
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function onConsoleTerminate(ConsoleTerminateEvent $event)
    {
        $commandName = $event->getCommand()->getName();

        $isNoticeSlack = false;
        $summaryData = [];
        $conditionMT = ' (type = :typeFirst or type = :typeDaily) and is_success = :isSuccess and created_date = :date ';

        if ('subs:mt:game:dtac:send-by-day' === $commandName) {
            //get summary info from table 'mt_dtac'
            $isNoticeSlack = true;

            $sql = 'select sum(count) from mt_dtac where '.$conditionMT;
            $stmt = $this->conn->executeQuery(
                $sql,
                [
                    'typeFirst' => MT::MT_TYPE_FIRST,
                    'typeDaily' => MT::MT_TYPE_DAILY,
                    'isSuccess' => 1,
                    'date' => DateTime::getLocalTime('Y-m-d')
                ]
            );

            //today sent
            $todaySent = intval($stmt->fetchColumn(0));

            //yesterday sent
            $stmt = $this->conn->executeQuery(
                $sql,
                [
                    'typeFirst' => MT::MT_TYPE_FIRST,
                    'typeDaily' => MT::MT_TYPE_DAILY,
                    'isSuccess' => 1,
                    'date' => (new \DateTime())->sub(new \DateInterval('P1D'))->format('Y-m-d')
                ]
            );

            $yesterdaySent = intval($stmt->fetchColumn(0));
            $diff = abs($todaySent - $yesterdaySent);
            $summaryData = [
                'todaySent' => number_format($todaySent),
                'yesterdaySent' => number_format($yesterdaySent),
                'diff' => number_format($diff),
            ];
        }

        if ('subs:mt:game:send-by-day' === $commandName) {
            //get summary info from table 'mt'
            $isNoticeSlack = true;

            $sql = 'select count(id) as count from mt where '.$conditionMT;
            $stmt = $this->conn->executeQuery(
                $sql,
                [
                    'typeFirst' => MT::MT_TYPE_FIRST,
                    'typeDaily' => MT::MT_TYPE_DAILY,
                    'isSuccess' => 1,
                    'date' => DateTime::getLocalTime('Y-m-d')
                ]
            );
            //today sent
            $todaySent = intval($stmt->fetchColumn(0));

            //yesterday sent
            $stmt = $this->conn->executeQuery(
                $sql,
                [
                    'typeFirst' => MT::MT_TYPE_FIRST,
                    'typeDaily' => MT::MT_TYPE_DAILY,
                    'isSuccess' => 1,
                    'date' => (new \DateTime())->sub(new \DateInterval('P1D'))->format('Y-m-d')
                ]
            );

            $yesterdaySent = intval($stmt->fetchColumn(0));
            $diff = abs($todaySent - $yesterdaySent);
            $summaryData = [
                'todaySent' => number_format($todaySent),
                'yesterdaySent' => number_format($yesterdaySent),
                'diff' => number_format($diff),
            ];
        }

        if ($isNoticeSlack) {
            $this->logger->noticeSlack(
                'Command kết thúc!',
                array_merge(
                    [
                        'command' => $commandName,
                        'code' => $event->getExitCode()
                    ],
                    $summaryData
                )
            );

            //Check diff
            if ($diff > self::DIFF_LIMIT) {
                $summaryData['command'] = $commandName;

                $this->logger->noticeSlack(
                    'MT được gửi ngày hôm nay lệch quá nhiều so với ngày hôm qua!',
                    $summaryData
                );
            }
        }
    }

    public function onMTOverDiff(GenericEvent $event)
    {
        $this->logger->noticeSlack('MT được gửi ngày hôm nay lệch quá nhiều so với ngày hôm qua!', $event->getArguments());
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            ConsoleEvents::EXCEPTION => ['onConsoleException'],
            ConsoleEvents::TERMINATE => ['onConsoleTerminate'],
            AppEvents::SEND_MT_OVER_DIFF => ['onMTOverDiff'],
//            ConsoleEvents::COMMAND => ['onConsoleCommand'],
        ];
    }

    /**
     * Unlock command
     *
     * @param $commandName
     */
    private function unlockCommand($commandName)
    {
        $lock = new LockHandler($commandName);
        $lock->release();
    }
}
