<?php

namespace App\MO;

use App\Util\Common;

class MO
{
    const MO_CHANNEL_SMS = 0;
    const MO_CHANNEL_IVR = 1;
    const MO_CHANNEL_WAP = 2;

    const MO_TRACK_API = 'http://pub.macrolenz.com/api/mo'; //@todo: change this
    const MO_TRACK_API_TOKEN = '2ckYHjcXub!bg?AK';

    public static $TEXTS = [
        'register' => ['OK', '*454154502', 'WIN', '*454154501', 'GAMES', '*454154503'], //add other keys here
        'unregistered' => ['STOP OK', 'STOP WIN', 'STOP GAMES'],
        'exception' => ['*454154596', '*454154597', '*454154598'],
    ];

    public static $TEXT_KEYWORD = [
        '*454154503' => self::KEYWORD_GAMES,
        '*454154596' => self::KEYWORD_GAMES,

        '*454154502' => self::KEYWORD_OK,
        '*454154597' => self::KEYWORD_OK,

        '*454154501' => self::KEYWORD_WIN,
        '*454154598' => self::KEYWORD_WIN

    ];

    const KEYWORD_OK = 'OK';
    const KEYWORD_WIN = 'WIN';
    const KEYWORD_GAMES = 'GAMES';

    public static $GAME_KEYWORDS = [self::KEYWORD_OK, self::KEYWORD_WIN, self::KEYWORD_GAMES];

    public static $SERVICES = [
        self::KEYWORD_OK => [
            'username' => 'skyads',
            'password' => 'skyads705',
            'price' => '6',
            'shortcode' => '4541545',
            'msg_welcome' => '0E020E2D0E1A0E040E380E130E170E350E480E2A0E210E310E040E2300A000480054004D004C0035002000670061006D0065002000360E1A002E002F0E020E490E2D0E040E270E320E2100200E2A0E2D0E1A0E160E320E2100A000300032003100310035003800380031003400A00E220E010E400E250E340E010E010E140020002A00310033003700A00E420E170E230E2D0E2D0E01',
            'texts' => [
                'register' => ['OK'],
                'unregistered' => ['STOP OK'],
            ],
        ],
        self::KEYWORD_WIN => [
            'username' => 'skyads',
            'password' => 'skyads705',
            'price' => '6',
            'shortcode' => '4541545',
            'msg_welcome' => '0E020E2D0E1A0E040E380E130E170E350E480E2A0E210E310E040E2300A000480054004D004C0035002000670061006D0065002000360E1A002E002F0E020E490E2D0E040E270E320E2100200E2A0E2D0E1A0E160E320E2100A000300032003100310035003800380031003400A00E220E010E400E250E340E010E010E140020002A00310033003700A00E420E170E230E2D0E2D0E01',
            'texts' => [
                'register' => ['WIN'],
                'unregistered' => ['STOP WIN'],
            ],
        ],
        self::KEYWORD_GAMES => [
            'username' => 'skyads',
            'password' => 'skyads705',
            'price' => '6',
            'shortcode' => '4541545',
            'msg_welcome' => '0E020E2D0E1A0E040E380E130E170E350E480E2A0E210E310E040E2300A000480054004D004C0035002000670061006D0065002000360E1A002E002F0E020E490E2D0E040E270E320E2100200E2A0E2D0E1A0E160E320E2100A000300032003100310035003800380031003400A00E220E010E400E250E340E010E010E140020002A00310033003700A00E420E170E230E2D0E2D0E01',
            'texts' => [
                'register' => ['GAMES'],
                'unregistered' => ['STOP GAMES'],
            ],
        ],
        //can add others mo_service key here

    ];

    /**
     * Check text given is register
     *
     * @param $text
     *
     * @return bool
     */
    public static function isRegister($text)
    {
        return in_array(strtoupper(trim($text)), self::$TEXTS['register']);
    }

    /**
     * Check text given is unregistered
     *
     * @param $text
     *
     * @return bool
     */
    public static function isUnregistered($text)
    {
        return in_array(strtoupper(trim($text)), self::$TEXTS['unregistered']);
    }

    /**
     * Check text given is exception
     *
     * @param $text
     *
     * @return bool
     */
    public static function isException($text)
    {
        return in_array(strtoupper(trim($text)), self::$TEXTS['exception']);
    }

    /**
     * Check text is valid
     *
     * @param $text
     *
     * @return bool
     */
    public static function isValidKeyword($text)
    {
        return self::isRegister($text) || self::isUnregistered($text) || self::isException($text);
    }

    /**
     * Check params is valid
     *
     * @param $data
     *
     * @return bool
     */
    public static function isValidParams($data)
    {
        $data = array_change_key_case($data, CASE_LOWER);

        if (!isset($data['from']) ||
            !isset($data['text']) ||
            !isset($data['time']) ||
            !isset($data['shortcode']) ||
            (!isset($data['moid']) && !isset($data['msgid'])) ||
            !isset($data['telcoid'])
        ) {
            return false;
        }

        return true;
    }

    public static function isInBlacklist($phone)
    {
        return in_array($phone, Common::file2Array(__DIR__.'/../../../config/blacklist.txt'));
    }
} 
