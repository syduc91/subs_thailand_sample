<?php

namespace App\MT;

use App\Telco;
use App\Util\Common;
use GuzzleHttp\Client;

class MTSender
{
    const MAX_RETRY = 0;
    const CONNECTION_TIMEOUT = 20;
    const RESPONSE_TIMEOUT = 20;

    private $api;
    private $params;
    /** @var  Client */
    private $client;
    private $url;

    public function __construct($api, $username, $password, $from)
    {
        $this->api = $api;
        $this->params['user'] = $username;
        $this->params['pass'] = $password;
        $this->params['from'] = $from;

        $this->client = new Client([
            'allow_redirects' => true,
            'timeout' => self::RESPONSE_TIMEOUT, //Response timeout, value 0 => no limit timeout
            'connect_timeout' => self::CONNECTION_TIMEOUT, //Connection timeout, value 0 => no limit timeout
        ]);
    }

    /**
     * Send MT with params given
     *
     * @param  string $to
     * @param  string $text
     * @param  string $keyword
     * @param  string $telcoId
     * @param         $cat
     * @param         $type
     * @param         $price
     * @param         $moId
     * @param bool    $isWelcome
     *
     * @return string
     */
    public function send($to, $text, $keyword, $telcoId, $cat, $type, $price, $moId, $isWelcome = false)
    {
        $this->addParam('price', $isWelcome ? '0' : $price);
        $this->addParam('to', $to);
        $this->addParam('text', $text);
        $this->addParam('telcoid', $telcoId);
        $this->addParam('keyword', $keyword);
        $this->addParam('cat', $cat);
        $this->addParam('type', $type);

        //DTAC no need param `linkid`
        if (Telco::DTAC != $telcoId) {
            $this->addParam('linkid', $moId);
        }

        $this->url = $this->api.'?'.Common::buildQueryString($this->params);
        $response = $this->client->get($this->url);
        var_dump($response);

        return $response->getBody()->getContents();
    }

    public function reset()
    {
        $this->params['price'] = null;
        $this->params['to'] = null;
        $this->params['text'] = null;
        $this->params['telcoid'] = null;
        $this->params['keyword'] = null;
        $this->params['cat'] = null;
        $this->params['type'] = null;
        $this->params['linkid'] = null;

        return $this;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function getUrl()
    {
        return $this->url;
    }

    private function addParam($key, $value)
    {
        $this->params[$key] = $value;

        return $this;
    }
} 
