<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 13/09/2016
 * Time: 11:54 AM
 */

namespace App\MT;

use App\MO\MO;
use App\Telco;

class MT
{
    const MT_TYPE_WELCOME = 'welcome';
    const MT_TYPE_FIRST = 'first';
    const MT_TYPE_DAILY = 'daily';
    const MT_TYPE_WEEKLY = 'weekly';
    const MT_TYPE_RETRY = 'retry';

    const MT_STATUS_SUCCESS = 'success';
    const MT_STATUS_FAIL = 'fail';

    const CAT_MT_WELCOME = 1;
    const CAT_MT_FIRST = 2;
    const CAT_MT_DAILY = 6;

    const MT_CONTENT_TYPE_DAILY = 5;
    const MT_CONTENT_TYPE_WELCOME = 1;

    const LIMIT_CHECK_DIFF = 1000;

    public static function getCatWelcomeMT($telcoId)
    {
        return 1;
    }

    public static function getTypeWelcomeMT($telcoId)
    {
        return 5;
    }

    public static function getCatFirstMT($telcoId)
    {
        if (Telco::AIS == $telcoId) {
            return 6;
        }

        if (Telco::TRUEMOVE == $telcoId) {
            return 2;
        }

        return null;
    }

    public static function getTypeFirstMT($telcoId)
    {
        return 0;
    }

    public static function getTypeDailyMT($telcoId)
    {
        return 0;
    }

    public static function getCatDailyMT($telcoId)
    {
        return 6;
    }
}
