<?php
namespace App;

final class Telco
{
    const DTAC = 1;
    const AIS = 3;
    const TRUEMOVE = 4;

    public static $TELCOS = [
        self::DTAC => 'DTAC',
        self::AIS => 'AIS',
        self::TRUEMOVE => 'TRUEMOVE',
    ];
} 
