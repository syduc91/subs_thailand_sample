<?php

namespace App;

final class AppEvents
{
    const RECEIVE_MO = 'receive_mo';
    const RECEIVE_MO_ERROR = 'receive_mo.error';
    const SEND_MT_SUCCESS = 'send_mt.success';
    const SEND_MT_ERROR = 'send_mt.error';
    const SEND_MT_OVER_DIFF = 'sent_mt.over_diff';
}
