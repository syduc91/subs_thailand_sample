<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Skyads\Logger\Logger;
use App\MO\MO;
use App\Util\Common;
use App\Telco;
use App\Util\GoogleAnalytics;
use Doctrine\DBAL\Connection;
use App\MT\MT;
use App\Util\DateTime;
use App\Util\Report\Label;
use App\Util\Report\Data;
use App\Util\Report\DN;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

//Request::setTrustedProxies(array('127.0.0.1'));

$app->get('/', function (Request $request) use ($app) {
    return 'homepage';
})->bind('homepage');

/**
 * Receive Subs MO Thailand
 */
$app->get('/subs/receive', function (Request $request) use ($app) {
    $params = !empty($request->query->all()) ? $request->query->all() : $request->request->all();
    $params = array_change_key_case($params, CASE_LOWER);
    if (empty($params)) {
        return '';
    }

    /** @var \Doctrine\DBAL\Connection $conn */
    $conn = $app['db'];

    if (isset($params['from'])) {
        $params['from'] = ltrim($params['from'], '+');

        //Do nothing if phone is in blacklist
        if (MO::isInBlacklist($params['from'])) {
            return '';
        }
    }

    if (isset($params['text'])) {
        $params['text'] = strtoupper(trim($params['text']));
    }

    //GA
    /** @var \App\Util\GoogleAnalytics $ga */
    $ga = $app['gg_analytics'];

    $keyword = strtoupper(getKeyword($params['text']));
    $action = GoogleAnalytics::MSG_MO_UNKNOWN;
    $isRegister = 0;
    if (MO::isRegister($params['text'])) {
        $action = GoogleAnalytics::MSG_MO_REGISTER;
        $isRegister = 1;
    } elseif (MO::isUnregistered($params['text'])) {
        $action = GoogleAnalytics::MSG_MO_UNREGISTERED;
    }
    $ga->sendEventMOReceive($action, $params['from'].'__'.json_encode($params));

    //Log
    /** @var Logger $logger */
    $logger = $app['subs.logger'];

    $logData = [
        'method' => $request->getMethod(),
        'params' => $params,
        'url' => $request->getSchemeAndHttpHost().$request->getRequestUri(),
    ];
    if ($request->isMethod('POST')) {
        $logData['url_get'] = $request->getSchemeAndHttpHost().$request->getPathInfo().'?'.Common::buildQueryString($params);
    }

    $dateMK = substr($params['time'], 0, 10);
    $logger->logMo('Nhận MO', $logData, false, $dateMK);

    //Test phone `66865333833`
    if ('66865333833' == $params['from']) {
        $logger->noticeSlack('Hú hú, sdt `66865333833` đang test!', $logData);
    }

    //Check params valid
    $errorsMsg = [];
    if (!MO::isValidParams($params)) {
        $errorsMsg[] = 'Params sai hoặc bị thiếu';
    }

    //If errors occurred from MK system
    if (!empty($errorsMsg)) {
        $msg = implode('; ', $errorsMsg);
        $logger->logMoError($msg, $logData, true, $dateMK);

        return '-1';
    }

    //Check keyword valid
    $isInvalidKeyword = false;
    if (!MO::isValidKeyword($params['text'])) {
        $logger->logMoError('Text (keyword) bị sai!', $logData, false, $dateMK);
        $isInvalidKeyword = true;
    }

    //Insert db
    //insert table `mo`
    $moid = isset($params['moid']) ? $params['moid'] : $params['msgid'];
    if (empty($moid)) {
        $moid = substr(uniqid('a', true), 0, 14); //fake
    }

    $isSystemError = false;
    $phone = $params['from'];
    $text = $params['text'];
    $keyword = getKeyword($text);
    $shortCode = $params['shortcode'];
    $telcoId = intval($params['telcoid']);
    $channel = isset($params['channel']) ? $params['channel'] : MO::MO_CHANNEL_SMS;

    //Handle check phone register duplicate
    if ($isRegister && (!$app['subs.accept_register_duplicate'] && !in_array($phone, $app['subs.phone_tested']))) {
        $sql = 'select phone from mo where phone = :phone limit 1';
        try {
            $stmt = $conn->executeQuery($sql, ['phone' => $params['from']]);
            if (!empty($stmt->fetchColumn(0))) {
                $conn->insert(
                    'mo_duplicate',
                    [
                        'mo_id' => $moid,
                        'phone' => $phone,
                        'keyword' => $keyword,
                        'text' => $text,
                        'shortcode' => $shortCode,
                        'time_request' => $params['time'],
                        'telco_id' => $telcoId,
                        'is_register' => $isRegister,
                        'channel' => $channel,
                        'time_utc' => DateTime::getUTCTime(),
                        'created_at' => DateTime::getLocalTime(),
                        'created_date' => DateTime::addHourLocalTime(DateTime::getLocalTime(), 1, 'Y-m-d'), //add 1 hour
                    ]
                );

                return '-1';
            }
        } catch (\Exception $ex) {
            $logger->logMoError('Ko insert MO info vào table `mo_duplicate` được. '.$ex->getMessage(), $logData, true, $dateMK);
            $isSystemError = true;
        }
    }

    //Fake moid, get real keyword if text is exception (*454154596 or *454154597 or *454154598)
    if (MO::isException($text)) {
        $keyword = MO::$TEXT_KEYWORD[$text]; //get keyword from map
        $moid = substr(uniqid('a', true), 0, 14); //fake
    }

    //Fake moid if text is unregistered && moid existed
    if (MO::isUnregistered($text)) {
        $sql = 'SELECT mo_id FROM mo WHERE mo_id = :moId';
        try {
            $stmt = $conn->executeQuery($sql, ['moId' => $moid]);
            $moCheck = $stmt->fetchColumn(0);
            if (!empty($moCheck)) {
                $moid = $moid.'_'.substr(uniqid('a', true), 0, 14); //fake
                $logData['params']['fake_moid'] = $moid;
            }
        } catch (\Exception $ex) {

        }
    }

    try {
        $conn->insert(
            'mo',
            [
                'mo_id' => $moid,
                'phone' => $phone,
                'keyword' => $keyword,
                'text' => $text,
                'shortcode' => $shortCode,
                'time_request' => $params['time'],
                'telco_id' => $telcoId,
                'is_register' => $isRegister,
                'channel' => $channel,
                'time_utc' => DateTime::getUTCTime(),
                'created_at' => DateTime::getLocalTime(),
                'created_date' => DateTime::addHourLocalTime(DateTime::getLocalTime(), 1, 'Y-m-d'), //add 1 hour
            ]
        );

    } catch (\Exception $ex) {
        $logger->logMoError('Ko insert MO info vào db được. '.$ex->getMessage(), $logData, true, $dateMK);
        $isSystemError = true;
    }

    //If keyword is `unregistered service` or exception => remove user from db
    if (MO::isUnregistered($text) || MO::isException($text) || $isInvalidKeyword) {
        try {
            $conn->delete('user', ['phone' => $phone]);

        } catch (\Exception $ex) {
            $logger->logMoError('Lỗi khi xóa user. '.$ex->getMessage(), $logData, true, $dateMK);
        }

        return '-1';
    }

    try {
        $conn->insert(
            'user',
            [
                'phone' => $phone,
                'created_at' => DateTime::getLocalTime(),
                'time_utc' => DateTime::getUTCTime(),
                'created_date' => DateTime::addHourLocalTime(DateTime::getLocalTime(), 1, 'Y-m-d'), //add 1 hour
                'telco_id' => $telcoId,
                'mo_id' => $moid,
                'keyword' => $keyword,
            ]
        );
    } catch (\Exception $ex) {
        $isSystemError = true;

        $logger->logMoError('Không insert đc user info vào db. '.$ex->getMessage(), $logData, true, $dateMK);
    }

    //If errors occurred from our system
    if ($isSystemError) {
        return '-1'; //@todo: chi fix tam thoi
        //return 'Có lỗi từ hệ thống!!!';
    }

    //Response
    return '-1'; //default

})->method('GET|POST')->bind('receive_mo_thai');

/**
 * Add a aff info
 */
$app->get('/aff/add', function (Request $request) use ($app) {
    $secretKey = $request->query->get('key');
    $clickId = $request->query->get('click_id');
    $phone = $request->query->get('phone');
    $pub = $request->query->get('pub');

    if (empty($secretKey) || $app['app.secret_key'] !== $secretKey) {
        throw new BadRequestHttpException('Missing or invalid params.');
    }

    /** @var Connection $conn */
    $conn = $app['db'];

    /** @var Logger $logger */
    $logger = $app['subs.logger'];

    //Insert `aff_access`
    try {
        $conn->insert(
            'aff_access',
            [
                'click_id' => $clickId,
                'phone' => $phone,
                'pub' => $pub,
                'created_at' => DateTime::getLocalTime(),
                'created_date' => DateTime::getLocalTime('Y-m-d'),
            ]
        );
    } catch (\Exception $ex) {
        $logger->noticeSlack('Caught error when insert table `aff_access`. Error: '.$ex->getMessage());
    }

    //Insert `aff`
    if (!empty($phone) && !empty($clickId)) {
        try {
            $conn->insert(
                'aff',
                [
                    'click_id' => $clickId,
                    'phone' => $phone,
                    'pub' => $pub,
                    'created_at' => DateTime::getLocalTime(),
                    'created_date' => DateTime::getLocalTime('Y-m-d'),
                ]
            );
        } catch (\Exception $ex) {
            $conn->update(
                'aff',
                [
                    'click_id' => $clickId,
                    'pub' => $pub,
                    'last_updated_at' => DateTime::getLocalTime(),
                ],
                [
                    'phone' => $phone,
                ]
            );
        }
    }

    return 'ok';
})->bind('add_aff');

/**
 * Check phone of aff is valid
 */
$app->get('/aff/check', function (Request $request) use ($app) {
    $secretKey = $request->query->get('key');
    $phone = $request->query->get('phone');

    if (empty($secretKey) || $app['app.secret_key'] !== $secretKey || empty($phone)) {
        throw new BadRequestHttpException('Missing or invalid params.');
    }

    /** @var Connection $conn */
    $conn = $app['db'];

    $minCheck = 1;
    $sql = 'select phone, count(phone) from aff_access where date(created_at) = :date and phone = :phone group by phone having count(phone) > '.$minCheck;
    $stmt = $conn->executeQuery(
        $sql,
        [
            'date' => DateTime::getLocalTime('Y-m-d'),
            'phone' => $phone,
        ]
    );

    $status = 'invalid';
    if (empty($stmt->fetch(\PDO::FETCH_ASSOC))) {
        $status = 'valid';
    }

    return json_encode(['status' => $status]);
})->bind('aff_check');

/**
 * Update info for aff (Eg. telco_id)
 */
$app->get('/aff/update', function (Request $request) use ($app) {
    $secretKey = $request->query->get('key');
    $phone = $request->query->get('phone');

    if (empty($secretKey) || $app['app.secret_key'] !== $secretKey || empty($phone)) {
        throw new BadRequestHttpException('Missing or invalid params.');
    }

    /** @var Connection $conn */
    $conn = $app['db'];
    /** @var Logger $logger */
    $logger = $app['subs.logger'];

    $request->query->remove('key');
    $request->query->remove('phone');
    $request->query->remove('pub');

    //Handle params
    $whitelistKeys = ['telco_id']; //Add other keys here
    $rawParams = $request->query->all();
    $validParams = [];
    foreach ($whitelistKeys as $field) {
        if (isset($rawParams[$field])) {
            $validParams[$field] = $rawParams[$field];
        }
    }

    //Update aff_access
    if (!empty($validParams)) {
        try {
            $conn->update(
                'aff_access',
                $validParams,
                [
                    'phone' => $phone,
                ]
            );

            //Update aff
            $conn->update(
                'aff',
                $validParams,
                [
                    'phone' => $phone,
                ]
            );
        } catch (\Exception $ex) {
            $logger->noticeSlack('Caught error when update table `aff_access`, `aff`. Error: '.$ex->getMessage());
        }
    }

    return 'ok';
})->bind('aff_update_telco');

/**
 * Report for track
 */
$app->get('/subs/report/track', function (Request $request) use ($app) {
    $params = $request->query->all();
    $params = array_change_key_case($params, CASE_LOWER);
    if (!isset($params['start']) || !isset($params['end']) || !isset($params['keyword'])) {
        return 'Params phải đầy đủ `keyword`, `start` và `end`. Định dạng là dd-mm-yyyy. VD: 19-09-2016';
    }

    if (
        false === strpos($params['start'], '-') ||
        false !== strpos($params['start'], '/') ||
        3 !== count(explode('-', $params['start'])) ||
        false === strpos($params['end'], '-') ||
        false !== strpos($params['end'], '/') ||
        3 !== count(explode('-', $params['end']))
    ) {
        return 'Param `start` hoặc `end` sai định dạng. Định dạng đúng phải là: dd-mm-yyyy';
    }

    $unit = isset($params['unit']) ? strtolower($params['unit']) : 'day'; //unit = day | week | month
    $pub = isset($params['pub']) ? strtolower($params['pub']) : 'ecomobi';

    if (!in_array($unit, DateTime::$DIFF_TYPE_SUPPORT)) {
        return 'Param `unit` ko đúng. Chọn 1 trong các giá trị: day, week, month';
    }

    $telco = !empty($params['telco']) ? $params['telco'] : 'all';
    $labels = Label::getLabels($params['start'], $params['end'], $unit);

    /** @var Connection $conn */
    $conn = $app['db'];
    $reportData = new Data($conn);

    //----- MO REGISTER ---------------
    //Get MO register summary
    $moRegSummaryData = $reportData->getMOTrackRegister($params['keyword'], $params['start'], $params['end'], $pub, $unit);
    $moRegSummary = Data::formatDataSummary($labels, $moRegSummaryData, $unit);

    //Get MO register summary today
    $moRegTodayData = $reportData->getMOTrackRegister(
        $params['keyword'],
        DateTime::getLocalTime('Y-m-d'),
        DateTime::getLocalTime('Y-m-d'),
        $pub,
        DateTime::DIFF_TYPE_DAY
    );
    $moRegToday = isset($moRegTodayData[0]['count']) ? intval($moRegTodayData[0]['count']) : 0;

    //Get MO register filter by telco id Today
    $moRegTelcoTodayData = $reportData->getMOTrackTelcoRegister(
        $params['keyword'],
        DateTime::getLocalTime('Y-m-d'),
        DateTime::getLocalTime('Y-m-d'),
        $pub,
        $telco,
        DateTime::DIFF_TYPE_DAY
    );
    $moRegTelcoToday = Data::formatDataTelco([DateTime::getLocalTime('d-m-Y')], $moRegTelcoTodayData, DateTime::DIFF_TYPE_DAY);

    //----- END MO REGISTER ---------------

    //----- MO UNREGISTERED ---------------
    //Get MO unregistered summary
    $moUnregSummaryData = $reportData->getMOTrackUnregistered($params['keyword'], $params['start'], $params['end'], $pub, $unit);
    $moUnregSummary = Data::formatDataSummary($labels, $moUnregSummaryData, $unit);

    //Get MO unregistered summary today
    $moUnregTodayData = $reportData->getMOTrackUnregistered(
        $params['keyword'],
        DateTime::getLocalTime('Y-m-d'),
        DateTime::getLocalTime('Y-m-d'),
        $pub,
        DateTime::DIFF_TYPE_DAY);
    $moUnRegToday = isset($moUnregTodayData[0]['count']) ? intval($moUnregTodayData[0]['count']) : 0;

    //Get MO unregistered filter by telco id Today
    $moUnRegTelcoTodayData = $reportData->getMOTrackTelcoUnregistered(
        $params['keyword'],
        DateTime::getLocalTime('Y-m-d'),
        DateTime::getLocalTime('Y-m-d'),
        $pub,
        $telco,
        DateTime::DIFF_TYPE_DAY
    );
    $moUnRegTelcoToday = Data::formatDataTelco([DateTime::getLocalTime('d-m-Y')], $moUnRegTelcoTodayData, DateTime::DIFF_TYPE_DAY);

    //Get clickId summary
    $clickSummaryData = $reportData->getClickId($params['start'], $params['end'], $pub, $unit);
    $clickSummary = Data::formatDataSummary($labels, $clickSummaryData, $unit);

    //Get clickId today
    $clickTodayData = $reportData->getClickId(
        DateTime::getLocalTime('Y-m-d'),
        DateTime::getLocalTime('Y-m-d'),
        $pub,
        DateTime::DIFF_TYPE_DAY
    );
    $clickToday = isset($clickTodayData[0]['count']) ? intval($clickTodayData[0]['count']) : 0;

    $moRegTelco = [];
    $moUnregTelco = [];

    if ('all' != $telco) {
        //Get mo register filter by telco id
        $moRegTelcoData = $reportData->getMOTrackTelcoRegister($params['keyword'], $params['start'], $params['end'], $pub, $telco, $unit);
        $moRegTelco = Data::formatDataTelco($labels, $moRegTelcoData, $unit);

        //Get MO unregistered filtered by telco
        $moUnregTelcoData = $reportData->getMOTrackTelcoUnregistered($params['keyword'], $params['start'], $params['end'], $pub, $telco, $unit);
        $moUnregTelco = Data::formatDataTelco($labels, $moUnregTelcoData, $unit);

        //Total filter by telco
        $totalRegTelco = [];
        $telcos = [];
        foreach ($moRegTelco as $telco => $data) {
            $telcos[] = $telco;
            $totalRegTelco[$telco] = number_format(array_sum($data));
        }

        $totalUnregTelco = [];
        foreach ($moUnregTelco as $telco => $data) {
            $totalUnregTelco[$telco] = number_format(array_sum($data));
        }

        $totalClickIdTelco = [];
        foreach ($telcos as $telco) {
            $totalClickIdTelco[$telco] = number_format(array_sum($clickSummary));
        }
    }

    //Total
    //Summary
    $totalReg = number_format(array_sum($moRegSummary));
    $totalUnreg = number_format(array_sum($moUnregSummary));
    $totalClickId = number_format(array_sum($clickSummary));

    if (DateTime::DIFF_TYPE_WEEK == $unit) {
        $labels = Label::formatWeekLabels($labels, 'd-m');
    }

    $response = [
        'labels' => $labels,
        'unit' => $unit,
        'extra' => [
            'today' => [
                'mo reg' => [
                    'all' => number_format($moRegToday),
                    'telco' => $moRegTelcoToday,
                ],
                'mo unreg' => [
                    'all' => number_format($moUnRegToday),
                    'telco' => $moUnRegTelcoToday,
                ],
                'click id' => [
                    'all' => number_format($clickToday),
                    'telco' => [],
                ],
            ],
            'total' => [
                'mo reg' => [
                    'all' => $totalReg, //
                    'telco' => [],
                ],
                'mo unreg' => [
                    'all' => $totalUnreg, //
                    'telco' => [],
                ],
                'click id' => [
                    'all' => $totalClickId, //
                    'telco' => [],
                ],
            ],
        ],
    ];

    if ('all' == $telco) {
        $response['data']['mo dk']['all'] = $moRegSummary;
        $response['data']['mo huy']['all'] = $moUnregSummary;
        $response['data']['click id']['all'] = $clickSummary;
    } else {
        $response['data']['mo dk']['telco'] = $moRegTelco;
        $response['data']['mo huy']['telco'] = $moUnregTelco;

        $response['extra']['total']['mo reg']['telco'] = $totalRegTelco;
        $response['extra']['total']['mo unreg']['telco'] = $totalUnregTelco;
        $response['extra']['total']['click id']['telco'] = $totalClickIdTelco;
    }

    return json_encode($response);

})->bind('subs_report_track');

/**
 * MT fail receiver
 */
$app->post('/mt-fail/receive', function (Request $request) use ($app) {
    $data = $request->request->all();

    /** @var Connection $conn */
    $conn = $app['db'];
    /** @var Logger $logger */
    $logger = $app['subs.logger'];

    $successCount = 0;
    $failCount = 0;

    foreach ($data as $index => $item) {
        $decoded = json_decode($item, true);

        if (!empty($decoded['time_request']) && !empty($decoded['phone'])) {
            //gen id
            $date = explode(' ', $decoded['time_request'])[0];

            try {
                //Check mysql timeout
                if (false === $conn->ping()) {
                    $conn->close();
                    $conn->connect();
                }

                $conn->insert(
                    'mt_retry',
                    [
                        'id' => $date.'_'.$decoded['phone'],
                        'time_request' => $decoded['time_request'],
                        'phone' => $decoded['phone'],
                        'shortcode' => !empty($decoded['shortcode']) ? $decoded['shortcode'] : null,
                        'telco_id' => !empty($decoded['telco_id']) ? $decoded['telco_id'] : null,
                        'status' => !empty($decoded['status']) ? $decoded['status'] : null,
                        'keyword' => !empty($decoded['keyword']) ? $decoded['keyword'] : null,
                        'retry' => 0,
                    ]
                );
                $successCount++;

            } catch (\Exception $ex) {
                $failCount++;
                if (false === strpos($ex->getMessage(), 'SQLSTATE[23000]')) { //duplicate exception
                    //handle normal exception here
                    $logger->noticeSlack('Lỗi khi nhận mt_try. '.$ex->getMessage());
                }
            }
        }
    }

    //Close mysql connection
    if (false !== $conn->ping()) {
        $conn->close();
    }

    return json_encode(
        [
            'total' => count($data),
            'success' => $successCount,
            'fail' => $failCount,
        ]
    );
});

/**
 * Test send MT
 */
$app->get('/mt/test', function (Request $request) use ($app) {
    $params = $request->query->all();

    $params = array_change_key_case($params, CASE_LOWER);
    if (empty($params)) {
        return '';
    }

    if (
        !isset($params['to']) ||
        !isset($params['text']) ||
        !isset($params['keyword']) ||
        !isset($params['telcoid']) ||
        !isset($params['price']) ||
        !isset($params['moid'])
    ) {
        return 'Thiếu params. Cần phải có các params sau: to, text, keyword, telcoid, price, moid';
    }

    $params['keyword'] = strtoupper($params['keyword']);
    $params['to'] = ltrim($params['to'], '+');

    $cat = (0 == $params['price']) ? MT::getCatWelcomeMT($params['telcoid']) : MT::getCatDailyMT($params['telcoid']);
    $type = (0 == $params['price']) ? MT::getTypeWelcomeMT($params['telcoid']) : MT::getTypeDailyMT($params['telcoid']);
    $isWelcome = (0 == $params['price']) ? true : false;

    //http://localhost:8888/mt/test?to=43526&text=link 1&keyword=ok&telcoid=1&price=3

    $keyword = $params['keyword'];
    $senderServiceId = 'mt_'.strtolower(trim($keyword)).'_sender';
    if (!isset($app[$senderServiceId])) {
        print '--> '.sprintf('Keyword %s is invalid!', $keyword).PHP_EOL;
        exit;
    }

    /** @var \App\MT\MTSender $mtSender */
    $mtSender = $app[$senderServiceId];
    $result = $mtSender->send(
        $params['to'],
        $params['text'],
        $params['keyword'],
        $params['telcoid'],
        $cat,
        $type,
        $params['price'],
        $params['moid'],
        $isWelcome
    );

    return $result;

})->bind('test-mt');

/**
 * Report
 */
$app->get('/subs/report', function (Request $request) use ($app) {
    $params = $request->query->all();
    $params = array_change_key_case($params, CASE_LOWER);
    if (!isset($params['start']) || !isset($params['end']) || !isset($params['keyword'])) {
        return 'Params phải đầy đủ `keyword`, `start` và `end`. Định dạng là dd-mm-yyyy. VD: 19-09-2016';
    }

    if (
        false === strpos($params['start'], '-') ||
        false !== strpos($params['start'], '/') ||
        3 !== count(explode('-', $params['start'])) ||
        false === strpos($params['end'], '-') ||
        false !== strpos($params['end'], '/') ||
        3 !== count(explode('-', $params['end']))
    ) {
        return 'Param `start` hoặc `end` sai định dạng. Định dạng đúng phải là: dd-mm-yyyy';
    }

    $unit = isset($params['unit']) ? strtolower($params['unit']) : 'day'; //unit = day | week | month
    if (!in_array($unit, DateTime::$DIFF_TYPE_SUPPORT)) {
        return 'Param `unit` ko đúng. Chọn 1 trong các giá trị: day, week, month';
    }

    $telco = !empty($params['telco']) ? $params['telco'] : 'all';

    $labels = Label::getLabels($params['start'], $params['end'], $unit);

    /** @var Connection $conn */
    $conn = $app['db'];
    $reportData = new Data($conn);

    //----- MO REGISTER ---------------
    //Get MO register summary today
    $moRegTodayData = $reportData->getMOReg(
        $params['keyword'],
        DateTime::getLocalTime('Y-m-d'),
        DateTime::getLocalTime('Y-m-d'),
        DateTime::DIFF_TYPE_DAY
    );
    $moRegToday = isset($moRegTodayData[0]['count']) ? intval($moRegTodayData[0]['count']) : 0;

    //Get MO register filter by telco id Today
    $moRegTelcoTodayData = $reportData->getMORegTelco(
        $params['keyword'],
        DateTime::getLocalTime('Y-m-d'),
        DateTime::getLocalTime('Y-m-d'),
        $telco,
        DateTime::DIFF_TYPE_DAY
    );
    $moRegTelcoToday = Data::formatDataTelco([DateTime::getLocalTime('d-m-Y')], $moRegTelcoTodayData, DateTime::DIFF_TYPE_DAY);
    //----- END MO REGISTER ---------------

    //----- MO UNREGISTERED ---------------
    //Get MO unregistered summary today
    $moUnregTodayData = $reportData->getMOUnreg(
        $params['keyword'],
        DateTime::getLocalTime('Y-m-d'),
        DateTime::getLocalTime('Y-m-d'),
        DateTime::DIFF_TYPE_DAY);
    $moUnRegToday = isset($moUnregTodayData[0]['count']) ? intval($moUnregTodayData[0]['count']) : 0;

    //Get MO unregistered filter by telco id Today
    $moUnRegTelcoTodayData = $reportData->getMOUnregTelco(
        $params['keyword'],
        DateTime::getLocalTime('Y-m-d'),
        DateTime::getLocalTime('Y-m-d'),
        $telco,
        DateTime::DIFF_TYPE_DAY
    );
    $moUnRegTelcoToday = Data::formatDataTelco([DateTime::getLocalTime('d-m-Y')], $moUnRegTelcoTodayData, DateTime::DIFF_TYPE_DAY);
    //----- END MO UNREGISTERED ---------------

    //----- MT SUCCESS ---------------
    //Get MT success summary today
    $mtOKSummaryTodayData = $reportData->getMTSuccess(
        $params['keyword'],
        DateTime::getLocalTime('Y-m-d'),
        DateTime::getLocalTime('Y-m-d'),
        DateTime::DIFF_TYPE_DAY);
    $mtOKSummaryToday = isset($mtOKSummaryTodayData[0]['count']) ? intval($mtOKSummaryTodayData[0]['count']) : 0;

    //Get MT success filter by telco id Today
    $mtOKTelcoTodayData = $reportData->getMTTelcoSuccess(
        $params['keyword'],
        DateTime::getLocalTime('Y-m-d'),
        DateTime::getLocalTime('Y-m-d'),
        $telco,
        DateTime::DIFF_TYPE_DAY
    );
    $mtOKTelcoToday = Data::formatDataTelco([DateTime::getLocalTime('d-m-Y')], $mtOKTelcoTodayData, DateTime::DIFF_TYPE_DAY);

    //----- END MT SUCCESS ---------------

    if ('all' != $telco) {
        //Get mo register filter by telco id
        $moRegTelcoData = $reportData->getMORegTelco($params['keyword'], $params['start'], $params['end'], $telco, $unit);
        $moRegTelco = Data::formatDataTelco($labels, $moRegTelcoData, $unit);

        //Get MO unregistered filtered by telco
        $moUnregTelcoData = $reportData->getMOUnregTelco($params['keyword'], $params['start'], $params['end'], $telco, $unit);
        $moUnregTelco = Data::formatDataTelco($labels, $moUnregTelcoData, $unit);

        //Get MT success telco
        $mtOKTelcoData = $reportData->getMTTelcoSuccess($params['keyword'], $params['start'], $params['end'], $telco, $unit);
        $mtOKTelco = Data::formatDataTelco($labels, $mtOKTelcoData, $unit);

        //Get MT fail filtered by telco
        $mtFailTelcoData = $reportData->getMTTelcoFail($params['keyword'], $params['start'], $params['end'], $telco, $unit);
        $mtFailTelco = Data::formatDataTelco($labels, $mtFailTelcoData, $unit);


        //Get User filtered by telco
        //$userTelcoData = $reportData->getUserTelco($params['keyword'], $params['start'], $params['end'], $telco, $unit);
        //$userTelco = Data::formatDataTelco($labels, $userTelcoData, $unit);

    } else {
        //Get MO register summary
        $moRegSummaryData = $reportData->getMOReg($params['keyword'], $params['start'], $params['end'], $unit);
        $moRegSummary = Data::formatDataSummary($labels, $moRegSummaryData, $unit);

        //Get MO unregistered summary
        $moUnregSummaryData = $reportData->getMOUnreg($params['keyword'], $params['start'], $params['end'], $unit);
        $moUnregSummary = Data::formatDataSummary($labels, $moUnregSummaryData, $unit);

        //Get MT success summary
        $mtOKSummaryData = $reportData->getMTSuccess($params['keyword'], $params['start'], $params['end'], $unit);
        $mtOKSummary = Data::formatDataSummary($labels, $mtOKSummaryData, $unit);

        //Get MT fail summary
        $mtFailSummaryData = $reportData->getMTFail($params['keyword'], $params['start'], $params['end'], $unit);
        $mtFailSummary = Data::formatDataSummary($labels, $mtFailSummaryData, $unit);

        //Get User summary
        //$userSummaryData = $reportData->getUser($params['keyword'], $params['start'], $params['end'], $unit);
        //$userSummary = Data::formatDataSummary($labels, $userSummaryData, $unit);
    }

    if (DateTime::DIFF_TYPE_WEEK == $unit) {
        $labels = Label::formatWeekLabels($labels, 'd-m');
    }

    $response = [
        'labels' => $labels,
        'unit' => $unit,
        'extra' => [
            'today' => [
                'mo reg' => [
                    'all' => $moRegToday,
                    'telco' => $moRegTelcoToday,
                ],
                'mo unreg' => [
                    'all' => $moUnRegToday,
                    'telco' => $moUnRegTelcoToday,
                ],
                'mt ok' => [
                    'all' => $mtOKSummaryToday,
                    'telco' => $mtOKTelcoToday,
                ],
            ],
        ],
    ];

    if ('all' == $telco) {
        $response['data']['mo dk']['all'] = $moRegSummary;
        $response['data']['mo huy']['all'] = $moUnregSummary;
        $response['data']['mt ok']['all'] = $mtOKSummary;
        $response['data']['mt fail']['all'] = $mtFailSummary;
        //$response['data']['user']['all'] = $userSummary;
    } else {
        $response['data']['mo dk']['telco'] = $moRegTelco;
        $response['data']['mo huy']['telco'] = $moUnregTelco;
        $response['data']['mt ok']['telco'] = $mtOKTelco;
        $response['data']['mt fail']['telco'] = $mtFailTelco;
        //$response['data']['user']['telco'] = $userTelco;
    }

    return json_encode($response);

})->bind('subs_report');

/**
 * Report for DN
 */
$app->get('/dn/report', function (Request $request) use ($app) {
    $params = $request->query->all();
    $params = array_change_key_case($params, CASE_LOWER);
    if (!isset($params['start']) || !isset($params['end']) || !isset($params['keyword'])) {
        return 'Params phải đầy đủ `keyword`, `start` và `end`. Định dạng là dd-mm-yyyy. VD: 19-09-2016';
    }

    if (
        false === strpos($params['start'], '-') ||
        false !== strpos($params['start'], '/') ||
        3 !== count(explode('-', $params['start'])) ||
        false === strpos($params['end'], '-') ||
        false !== strpos($params['end'], '/') ||
        3 !== count(explode('-', $params['end']))
    ) {
        return 'Param `start` hoặc `end` sai định dạng. Định dạng đúng phải là: dd-mm-yyyy';
    }

    $unit = isset($params['unit']) ? strtolower($params['unit']) : 'day'; //unit = day | week | month

    if (!in_array($unit, DateTime::$DIFF_TYPE_SUPPORT)) {
        return 'Param `unit` ko đúng. Chọn 1 trong các giá trị: day, week, month';
    }

    $telco = !empty($params['telco']) ? $params['telco'] : 'all';
    $labels = Label::getLabels($params['start'], $params['end'], $unit);

    /** @var Connection $conn */
    $conn = $app['db'];
    $report = new DN($conn);

    //Get DN success summary
    $dnSuccessSummaryData = $report->getChargeSuccess($params['keyword'], $params['start'], $params['end'], $unit);
    $dnSuccessSummary = Data::formatDataSummary($labels, $dnSuccessSummaryData, $unit);

    //Get DN success summary today
    $dnSuccessTodayData = $report->getChargeSuccess(
        $params['keyword'],
        DateTime::getLocalTime('Y-m-d'),
        DateTime::getLocalTime('Y-m-d'),
        DateTime::DIFF_TYPE_DAY
    );
    $dnSuccessToday = isset($dnSuccessTodayData[0]['count']) ? intval($dnSuccessTodayData[0]['count']) : 0;

    //Get DN success telco today
    $dnSuccessTelcoTodayData = $report->getChargeSuccessTelco(
        $params['keyword'],
        DateTime::getLocalTime('Y-m-d'),
        DateTime::getLocalTime('Y-m-d'),
        $telco,
        DateTime::DIFF_TYPE_DAY
    );
    $dnSuccessTelcoToday = Data::formatDataTelco([DateTime::getLocalTime('d-m-Y')], $dnSuccessTelcoTodayData, DateTime::DIFF_TYPE_DAY);

    if ('all' != $telco) {
        //Get DN success filter by telco id
        $dnSuccessTelcoData = $report->getChargeSuccessTelco($params['keyword'], $params['start'], $params['end'], $telco, $unit);
        $dnSuccessTelco = Data::formatDataTelco($labels, $dnSuccessTelcoData, $unit);
    }

    if (DateTime::DIFF_TYPE_WEEK == $unit) {
        $labels = Label::formatWeekLabels($labels, 'd-m');
    }

    $response = [
        'labels' => $labels,
        'unit' => $unit,
        'extra' => [
            'today' => [
                'mt charge success' => [
                    'all' => number_format($dnSuccessToday),
                    'telco' => $dnSuccessTelcoToday,
                ],
            ],
        ],
    ];

    if ('all' == $telco) {
        $response['data']['mt charge success']['all'] = $dnSuccessSummary;
    } else {
        $response['data']['mt charge success']['telco'] = $dnSuccessTelco;
    }

    return json_encode($response);

})->bind('subs_report_dn');

/**
 * Add links content
 */
$app->get('/links/add', function (Request $request) use ($app) {
    if ($request->isMethod('POST')) {
        $params = $request->request->all();

        /** @var Connection $conn */
        $conn = $app['db'];

        try {
            $conn->insert(
                'link',
                [
                    'keyword' => $params['keyword'],
                    'link' => $params['link'],
                ]
            );

            $app['session']->getFlashBag()->add('success', sprintf('Insert link: "%s", keyword: "%s" successful.', $params['link'], $params['keyword']));
        } catch (\Exception $ex) {
            $app['session']->getFlashBag()->add('warning', sprintf('Error while insert link: "%s", keyword: "%s".', $params['link'], $params['keyword']));
        }

        return $app->redirect($request->getBaseUrl().$request->getRequestUri(), 301);
    }

    return $app['twig']->render('links/add.twig', ['baseUrl' => $request->getBaseUrl()]);

})->method('GET|POST')->bind('add_link_content');

/**
 * Get list links content
 */
$app->get('/links', function (Request $request) use ($app) {
    /** @var Connection $conn */
    $conn = $app['db'];

    $page = (!empty($request->query->get('page'))) ? intval($request->query->get('page')) : 1;
    $page = ($page < 1) ? 1 : $page;
    $limit = (!empty($request->query->get('limit'))) ? intval($request->query->get('limit')) : 50;
    $limit = ($limit > 0) ? $limit : 50;

    $keyword = strtoupper(trim($request->query->get('keyword')));
    $keywordCondition = (!empty($keyword)) ? ' WHERE keyword = :keyword ' : '';

    $sqlCount = 'SELECT COUNT(id) FROM link '.$keywordCondition;
    $stmt = $conn->prepare($sqlCount);
    if (!empty($keyword)) {
        $stmt->bindParam('keyword', $keyword);
    }
    $stmt->execute();
    $total = (int)$stmt->fetchColumn(0);

    $offset = ($page - 1) * $limit;
    $totalPage = ceil($total / $limit);

    $sql = 'SELECT * FROM link '.$keywordCondition.' ORDER BY ID DESC LIMIT '.$limit.' OFFSET '.$offset;
    $stmt = $conn->prepare($sql);
    if (!empty($keyword)) {
        $stmt->bindParam('keyword', $keyword);
    }

    $stmt->execute();
    $links = $stmt->fetchAll();

    return $app['twig']->render('links/list.twig', [
        'links' => $links,
        'totalPage' => $totalPage,
        'currentPage' => $page,
        'keyword' => $keyword,
        'limit' => $limit,
        'baseUrl' => $request->getBaseUrl(),
        'count' => $total,
        'mainKeyword' => $app['main_keyword'],
    ]);

})->bind('list_links_content');

/**
 * Edit link content
 */
$app->match('/links/{id}/edit', function (Request $request, $id) use ($app) {
    /** @var Connection $conn */
    $conn = $app['db'];

    if ($request->isMethod('POST')) {
        $params = $request->request->all();

        $conn->update(
            'link',
            [
                'keyword' => $params['keyword'],
                'link' => $params['link'],
            ],
            [
                'id' => $params['id'],
            ]
        );

        $app['session']->getFlashBag()->add('success', sprintf('Update link id: "%s", keyword: "%s" successful.', $id, $params['keyword']));

        return $app->redirect($request->getBaseUrl().'/links', 301);
    }

    $sql = 'SELECT * FROM link WHERE id = :id';
    $stmt = $conn->prepare($sql);
    $stmt->bindParam('id', $id);
    $stmt->execute();
    $link = $stmt->fetch(\PDO::FETCH_ASSOC);
    if (empty($link)) {
        $app['session']->getFlashBag()->add('warning', sprintf('Link id: "%s" not found.', $id));

        return $app->redirect($request->getBaseUrl().'/links', 301);
    }

    return $app['twig']->render('links/edit.twig', [
        'link' => $link,
        'baseUrl' => $request->getBaseUrl(),
    ]);

})->method('GET|POST')->bind('edit_link_content');

/**
 * Delete link content
 */
$app->get('/links/{id}/delete', function (Request $request, $id) use ($app) {
    /** @var Connection $conn */
    $conn = $app['db'];

    $sql = 'SELECT * FROM link WHERE id = :id';
    $stmt = $conn->prepare($sql);
    $stmt->bindParam('id', $id);
    $stmt->execute();
    $link = $stmt->fetch(\PDO::FETCH_ASSOC);
    if (empty($link)) {
        $app['session']->getFlashBag()->add('warning', sprintf('Link id: "%s" not found.', $id));

        return $app->redirect($request->getBaseUrl().'/links', 301);
    }
    $keyword = $link['keyword'];
    $conn->delete('link', ['id' => $id]);
    $app['session']->getFlashBag()->add('success', sprintf('Link id: "%s", keyword: "%s" was deleted', $id, $keyword));

    return $app->redirect($request->getBaseUrl().'/links', 301);

})->bind('delete_link_content');

/**
 * Receive DN (Delivery Notification)
 */
$app->get('/dn/receive', function (Request $request) use ($app) {
    $params = array_change_key_case($request->query->all(), CASE_LOWER);
    if (empty($params)) {
        return '';
    }

    $paramsRequired = ['mtid', 'datetime', 'msisdn', 'shortcode', 'telcoid', 'countryid', 'status'];
    //if (!empty(array_diff($paramsRequired, array_keys($params)))) {
    //return 'Cần phải có đầy đủ params: '.implode(', ', $paramsRequired);
    //}

    /** @var Logger $logger */
    $logger = $app['subs.logger'];

    /** @var Connection $conn */
    $conn = $app['db'];

    $moId = !empty($params['moid']) ? $params['moid'] : null;
    $phone = !empty($params['msisdn']) ? $params['msisdn'] : null;
    $mtId = !empty($params['mtid']) ? $params['mtid'] : null;
    $datetime = !empty($params['datetime']) ? $params['datetime'] : null;
    $shortcode = !empty($params['shortcode']) ? $params['shortcode'] : null;
    $telcoId = !empty($params['telcoid']) ? $params['telcoid'] : null;
    $countryId = !empty($params['countryid']) ? $params['countryid'] : null;
    $status = !empty($params['status']) ? $params['status'] : null;

    try {
        //Get keyword by phone
        $sql = 'select keyword from mo where phone = :phone limit 1';
        $stmt = $conn->executeQuery($sql, ['phone' => $phone]);
        $keyword = $stmt->fetchColumn(0);

        $conn->insert(
            'mt_dn',
            [
                'mt_id' => $mtId,
                'mo_id' => $moId,
                'time_request' => $datetime,
                'phone' => $phone,
                'shortcode' => $shortcode,
                'telco_id' => $telcoId,
                'country_id' => $countryId,
                'status' => $status,
                'created_at' => DateTime::getLocalTime(),
                'time_utc' => DateTime::getUTCTime(),
                'created_date' => DateTime::getLocalTime('Y-m-d'),
                'is_charge_success' => DN::isChargeSuccess($params['telcoid'], $params['status']),
                'keyword' => $keyword,
            ]
        );

    } catch (\Exception $ex) {
        if (false === strpos($ex->getMessage(), 'Duplicate')) {
            $logger->noticeSlack('Có lỗi trong khi insert table mt_dn. '.$ex->getMessage());
        }
    }

    return 'ok';
})->bind('receive_dn');

$app->error(function (\Exception $e, Request $request, $code) {
    switch ($code) {
        case 404:
            $message = sprintf('{"code": %s, "message": "The requested page could not be found."}', $code);
            break;
        default:
            $message = sprintf('{"code": %s, "message": "We are sorry, but something went terribly wrong."}', $code);
    }

    return new Response($message);
});

/**
 * Insert a record to table `mt`
 *
 * @param Connection $conn
 * @param string     $phone
 * @param string     $moId
 * @param string     $from
 * @param string     $telcoId
 * @param string     $content
 * @param string     $keyword
 * @param null       $type
 * @param null       $status
 *
 * @return int
 */
function insertMT(Connection $conn, $phone, $moId, $from, $telcoId, $content, $keyword, $type = null, $status = null)
{
    return $conn->insert(
        'mt',
        [
            'phone' => $phone,
            'mo_id' => $moId,
            '`from`' => $from,
            'telco_id' => $telcoId,
            'content' => $content,
            'keyword' => $keyword,
            'type' => $type,
            'status' => $status,
            'created_at' => DateTime::getLocalTime(),
            'time_utc' => DateTime::getUTCTime(),
        ]
    );
}

/**
 * Update last mt time for user
 *
 * @param Connection $conn
 * @param            $phone
 *
 * @return int
 */
function updateLastMTTime(Connection $conn, $phone)
{
    return $conn->update(
        'user',
        [
            'last_mt_at' => DateTime::getLocalTime(),
            'last_mt_at_utc' => DateTime::getUTCTime(),
        ],
        [
            'phone' => $phone,
        ]
    );
}

/**
 * Get keyword from text
 *
 * @param string $text
 *
 * @return string
 */
function getKeyword($text)
{
    $text = trim($text);
    if (array_key_exists(strtoupper($text), MO::$TEXT_KEYWORD)) {
        return MO::$TEXT_KEYWORD[strtoupper($text)];
    }

    $parts = explode(' ', $text);
    if (1 === count($parts)) {
        return $text;
    }

    if (2 <= count($parts)) {
        return trim($parts[1]);
    }

    return null;
}