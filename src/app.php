<?php

use Silex\Application;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use App\Util\GoogleAnalytics;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\MonologServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\SessionServiceProvider;
use App\MT\MTSender;
use Skyads\Logger\Logger;
use App\MO\MO;
use App\Util\ContentManager;
use App\EventListener\CommandSubscriber;

$app = new Application();
$app->register(new ServiceControllerServiceProvider());
$app->register(new HttpFragmentServiceProvider());
$app->register(new DoctrineServiceProvider());
$app->register(new MonologServiceProvider());
$app->register(new SessionServiceProvider());

$app->register(new TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/Resources/views',
));

//Logger setting

$app['subs.logger'] = function () use ($app) {
    return new Logger(realpath(__DIR__.'/../var/logs'), $app['slack.api_token'], $app['slack.chanel'], $app['slack.username'], $app['env'], 0777, 'nginx', 'syduc');
};
//------------------------------

//GA
$app['gg_analytics'] = function () use ($app) {
    return new GoogleAnalytics($app['ga_id'], $app['ga_domain']);
};

//Content Manager
$app['subs.content_manager'] = function () use ($app) {
    return new ContentManager($app['db']);
};

//OK sender
$app['mt_ok_sender'] = function () use ($app) {
    return new MTSender(
        $app['mt_api'],
        MO::$SERVICES[MO::KEYWORD_OK]['username'],
        MO::$SERVICES[MO::KEYWORD_OK]['password'],
        MO::$SERVICES[MO::KEYWORD_OK]['shortcode']
    );
};

//WIN sender
$app['mt_win_sender'] = function () use ($app) {
    return new MTSender(
        $app['mt_api'],
        MO::$SERVICES[MO::KEYWORD_WIN]['username'],
        MO::$SERVICES[MO::KEYWORD_WIN]['password'],
        MO::$SERVICES[MO::KEYWORD_WIN]['shortcode']
    );
};

//GAMES sender
$app['mt_games_sender'] = function () use ($app) {
    return new MTSender(
        $app['mt_api'],
        MO::$SERVICES[MO::KEYWORD_GAMES]['username'],
        MO::$SERVICES[MO::KEYWORD_GAMES]['password'],
        MO::$SERVICES[MO::KEYWORD_GAMES]['shortcode']
    );
};

$app['subs.subscriber.command'] = function () use ($app) {
    $logger = clone $app['subs.logger'];
    $logger->setSlackChannel($app['slack.chanel_special']);

    return new CommandSubscriber($logger, $app['db']);
};

$app->before(function (\Symfony\Component\HttpFoundation\Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
});

$app['subs.http_client'] = function () use ($app) {
    return new \GuzzleHttp\Client([
        'allow_redirects' => true,
        'timeout' => MTSender::RESPONSE_TIMEOUT, //Response timeout, value 0 => no limit timeout
        'connect_timeout' => MTSender::CONNECTION_TIMEOUT, //Connection timeout, value 0 => no limit timeout
    ]);
};

$app['redis'] = function () use ($app) {
    $redis = new \Redis();
    $redis->pconnect($app['redis_host'], $app['redis_port'], $app['redis_timeout']);
    $redis->select($app['redis_db']);

    return $redis;
};

$app['duplicate_mt_checker'] = function () use ($app) {
    return new \App\Util\DuplicateMTChecker($app['redis']);
};

return $app;
