<?php

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Doctrine\DBAL\Connection;
use App\MT\MTSender;
use App\MO\MO;
use Skyads\Logger\Logger;
use App\Util\Common;
use App\Telco;
use App\MT\MT;
use App\Util\GoogleAnalytics;
use App\Util\DateTime;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\EventDispatcher\EventDispatcher;
use App\AppEvents;
use Symfony\Component\Filesystem\LockHandler;
use App\Util\DuplicateMTChecker;

$console = new Application('Subs Application', 'n/a');
$console->getDefinition()->addOption(new InputOption('--env', '-e', InputOption::VALUE_REQUIRED, 'The Environment name.', 'dev'));

/** @var EventDispatcher $dispatcher */
$dispatcher = $app['dispatcher'];
$dispatcher->addSubscriber($app['subs.subscriber.command']);
$console->setDispatcher($dispatcher);

/**
 * [Subs Thai - OK, WIN, GAMES] Send welcome MT. Create cron run 1 time / 1 minute
 *
 */
$console
    ->register('subs:mt:game:send-welcome')
    ->setDefinition(array(// new InputOption('some-option', null, InputOption::VALUE_NONE, 'Some help'),
        new InputArgument('keywords', InputArgument::OPTIONAL | InputArgument::IS_ARRAY, 'The keywords to be executed', MO::$GAME_KEYWORDS),
    ))
    ->setDescription('Send welcome MT for `Game` service Thailand')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        /** @var Connection $conn */
        $conn = $app['db'];
        $keywords = array_map('strtoupper', $input->getArgument('keywords'));

        $sql = 'SELECT * FROM user WHERE (telco_id = :telcoAIS or telco_id = :telcoTrueMove) '.
            ' AND welcome_mt_at is null and (time_to_sec(timediff(:now, created_at))/60 >= 2) AND keyword in ("'.implode('","', $keywords).'") '.
            ' ORDER BY created_at ASC ';

        $stmt = $conn->prepare($sql);
        $telcoAIS = Telco::AIS;
        $telcoTrueMove = Telco::TRUEMOVE;
        $now = DateTime::getLocalTime();

        $stmt->bindParam('telcoAIS', $telcoAIS);
        $stmt->bindParam('telcoTrueMove', $telcoTrueMove);
        $stmt->bindParam('now', $now);

        $stmt->execute();
        $users = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        /** @var Logger $logger */
        $logger = $app['subs.logger'];
        foreach ($users as $user) {
            $to = $user['phone'];
            $keyword = $user['keyword'];
            $telcoId = $user['telco_id'];
            $moId = $user['mo_id'];

            $output->writeln(sprintf('['.DateTime::getLocalTime().']'.' - Sending welcome MT for phone: %s, keyword: %s, telcoId: %s ...', $to, $keyword, $telcoId));

            $senderServiceId = 'mt_'.strtolower(trim($keyword)).'_sender';
            if (!isset($app[$senderServiceId])) {
                $output->writeln('--> '.sprintf('Keyword %s is invalid!', $keyword));
                continue;
            }

            $text = MO::$SERVICES[$keyword]['msg_welcome'];
            $shortCode = MO::$SERVICES[$keyword]['shortcode'];
            $price = MO::$SERVICES[$keyword]['price'];

            /** @var DuplicateMTChecker $checker */
            $checker = $app['duplicate_mt_checker'];

            if ($checker->isSentMT($keyword, $to, 0, $checker::TYPE_WELCOME)) {
                $output->writeln('--> '.sprintf('Welcome MT already sent for phone: %s, keyword: %s', $to, $keyword));
                $logger->logMtError(sprintf('Welcome MT already sent for phone: %s, keyword: %s', $to, $keyword), [], true);

                continue;
            }

            /** @var \App\MT\MTSender $mtSender */
            $mtSender = $app[$senderServiceId];

            $dataResult = trySendMT(
                $mtSender,
                $to,
                $text,
                $keyword,
                $telcoId,
                MT::getCatWelcomeMT($telcoId),
                MT::getTypeWelcomeMT($telcoId),
                $price,
                $moId,
                true,
                true
            ); //Force retry send MT 3 times if error

            $isSuccess = $dataResult['isSuccess'];
            $retry = $dataResult['retry'];
            $result = $dataResult['result'];
            $paramsSent = $dataResult['params'];

            $logData = [
                'result' => $result,
                'retry' => $retry,
                'url' => $dataResult['url'],
            ];

            //GA
            /** @var GoogleAnalytics $ga */
            $ga = $app['gg_analytics'];

            if ($isSuccess) {
                $action = GoogleAnalytics::MSG_MT_SEND_SUCCESS;
                $gaLabel = $result.'__'.json_encode($paramsSent);
            } else {
                $action = GoogleAnalytics::MSG_MT_SEND_FAIL;
                $gaLabel = $result.'__'.$to;
            }
            $ga->sendEventMT($action, $gaLabel);

            if ($isSuccess) {
                $logger->logMtSuccess('Send welcome MT success', $logData);

            } else {
                $logger->logMtError('Send welcome MT error', $logData, true);
                $output->writeln('--> Error!');
            }

            //Check mysql timeout
            if (false === $conn->ping()) {
                $conn->close();
                $conn->connect();
            }

            //Insert MT
            $status = $isSuccess ? MT::MT_STATUS_SUCCESS : MT::MT_STATUS_FAIL;
            try {
                $output->writeln('--> Insert a record into table `mt` success!');

                //Update field `welcome_at` for user
                $welcomeAt = DateTime::getLocalTime();
                $conn->update('user', ['welcome_mt_at' => $welcomeAt], ['phone' => $to]);
                $output->writeln('--> Update field `welcome_mt_at` = "'.$welcomeAt.'" success!');

                insertMT($conn, $to, $shortCode, $telcoId, $text, $keyword, MT::MT_TYPE_WELCOME, $status);
            } catch (\Exception $ex) {
                $logger->logMtError('Insert a record into table `mt` fail, because '.$ex->getMessage(), $logData, true);
                $output->writeln('--> Insert a record into table `mt` fail!');
            }

            $output->writeln('----------------------------------------------------------');
        }

        //Close connection
        if (false !== $conn->ping()) {
            $conn->close();
        }

        $output->writeln(sprintf('Total: %d', count($users)));
        $output->writeln('Done');

        return true;
    });

/**
 * [Subs Thai - OK, WIN, GAMES] Send first MT. Create cron run 1 time / 1 minute
 *
 */
$console
    ->register('subs:mt:game:send-first')
    ->setDefinition(array(// new InputOption('some-option', null, InputOption::VALUE_NONE, 'Some help'),
        new InputArgument('keywords', InputArgument::OPTIONAL | InputArgument::IS_ARRAY, 'The keywords to be executed', MO::$GAME_KEYWORDS),
    ))
    ->setDescription('Send first MT for `Game` service Thai')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        /** @var Connection $conn */
        $conn = $app['db'];

        /** @var Logger $logger */
        $logger = $app['subs.logger'];

        /** @var \App\Util\ContentManager $contentManager */
        $contentManager = $app['subs.content_manager'];
        $keywords = array_map('strtoupper', $input->getArgument('keywords'));

        $sql = 'SELECT * FROM user WHERE (telco_id = :telcoAIS or telco_id = :telcoTrueMove) '.
            ' AND welcome_mt_at is not null AND time_to_sec(timediff(:now, welcome_mt_at))/60 >= 1 '.
            ' AND has_first_mt = 0 AND keyword IN ("'.implode('","', $keywords).'") '.
            ' ORDER BY created_at ASC ';

        $stmt = $conn->prepare($sql);
        $telcoAIS = Telco::AIS;
        $telcoTrueMove = Telco::TRUEMOVE;
        $now = DateTime::getLocalTime();

        $stmt->bindParam('telcoAIS', $telcoAIS);
        $stmt->bindParam('telcoTrueMove', $telcoTrueMove);
        $stmt->bindParam('now', $now);

        $stmt->execute();
        $users = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        foreach ($users as $user) {
            $to = $user['phone'];
            $keyword = $user['keyword'];
            $telcoId = $user['telco_id'];
            $moId = $user['mo_id'];
            $price = MO::$SERVICES[$keyword]['price'];

            $output->writeln(sprintf('['.DateTime::getLocalTime().']'.' - Sending first MT for phone: %s, keyword: %s, telcoId = %s ...', $to, $keyword, $telcoId));

            $senderServiceId = 'mt_'.strtolower(trim($keyword)).'_sender';
            if (!isset($app[$senderServiceId])) {
                $output->writeln('--> '.sprintf('Keyword %s is invalid!', $keyword));
                continue;
            }

            /** @var DuplicateMTChecker $checker */
            $checker = $app['duplicate_mt_checker'];

            if ($checker->isSentMT($keyword, $to, $price, $checker::TYPE_FIRST)) {
                $output->writeln('--> '.sprintf('First MT already sent for phone: %s, keyword: %s', $to, $keyword));
                $logger->logMtError(sprintf('First MT already sent for phone: %s, keyword: %s', $to, $keyword), [], true);

                continue;
            }

            $lastLinkId = $user['last_link_id'];
            $nextLinkInfo = $contentManager->getNextLink($keyword, $lastLinkId);
            $text = $nextLinkInfo['link'];
            $shortCode = MO::$SERVICES[$keyword]['shortcode'];

            $newLastLinkId = $nextLinkInfo['id'];

            /** @var \App\MT\MTSender $mtSender */
            $mtSender = $app[$senderServiceId];

            $dataResult = trySendMT(
                $mtSender,
                $to,
                $text,
                $keyword,
                $telcoId,
                MT::getCatFirstMT($telcoId),
                MT::getTypeFirstMT($telcoId),
                $price,
                $moId,
                false,
                true
            ); //Force retry send MT 3 times if error

            $isSuccess = $dataResult['isSuccess'];
            $retry = $dataResult['retry'];
            $result = $dataResult['result'];
            $paramsSent = $dataResult['params'];

            //Make a request MO unregistered if response = 410 (Invalid Subscriber) or 406 (Blacklisted MSISDN)
            if ('410' == $result || '406' == $result) {
                $dateTime = DateTime::getLocalTime('Y-m-dH:i:s');
                //sendMO($app['subs_api_receive'], $user['phone'], 'STOP '.$user['keyword'], $dateTime, $shortCode, time(), $telcoId, $app['mt_channel']);

                $output->writeln(sprintf('--> Send request receive MO unregistered for %s', $user['phone']));
                $output->writeln('----------------------------------------------------------');
                //sleep(1);
                //continue;
            }

            $logData = [
                'result' => $result,
                'retry' => $retry,
                'url' => $dataResult['url'],
            ];

            //GA
            /** @var GoogleAnalytics $ga */
            $ga = $app['gg_analytics'];

            if ($isSuccess) {
                $action = GoogleAnalytics::MSG_MT_SEND_SUCCESS;
                $gaLabel = $result.'__'.$to;
            } else {
                $action = GoogleAnalytics::MSG_MT_SEND_FAIL;
                $gaLabel = $result.'__'.json_encode($paramsSent);
            }
            $ga->sendEventMT($action, $gaLabel);

            //Check mysql timeout
            if (false === $conn->ping()) {
                $conn->close();
                $conn->connect();
            }

            if ($isSuccess) {
                $logger->logMtSuccess('Send first MT success', $logData);
                $output->writeln('--> Send first MT success!');

            } else {
                $logger->logMtError('Send first MT error!', $logData, true);
                $output->writeln('--> Send first MT error!');
            }

            //Insert MT
            $status = $isSuccess ? MT::MT_STATUS_SUCCESS : MT::MT_STATUS_FAIL;
            try {
                //Update last_mt time for user
                $conn->update(
                    'user',
                    [
                        'has_first_mt' => 1,
                        'last_link_id' => $newLastLinkId,
                        'last_mt_at' => DateTime::getLocalTime(),
                        'last_mt_at_utc' => DateTime::getUTCTime(),
                        'last_mt_response' => $result,
                    ],
                    [
                        'phone' => $to,
                    ]
                );

                $output->writeln('--> Update last_mt time for user success!');
                $output->writeln('--> Update field `last_link_id` = '.$newLastLinkId.' success!');

                $output->writeln('--> Insert a record into table `mt` success!');
                insertMT($conn, $to, $shortCode, $telcoId, $text, $keyword, MT::MT_TYPE_FIRST, $status, $result);

            } catch (\Exception $ex) {
                $logger->logMtError('Insert a record into table `mt` fail, because '.$ex->getMessage(), $logData, true);
                $output->writeln('--> Insert a record into table `mt` fail!');
            }

            $output->writeln('----------------------------------------------------------');
        }

        //Close connection
        if (false !== $conn->ping()) {
            $conn->close();
        }
        $output->writeln(sprintf('Total: %d', count($users)));
        $output->writeln('Done');

        return true;
    });

/**
 * Fix logs permission. Create cron run 1 time / 1 minute
 */
$console
    ->register('subs:logs:fix-permission')
    ->setDefinition(array(// new InputOption('some-option', null, InputOption::VALUE_NONE, 'Some help'),
    ))
    ->setDescription('Fix permission for log files')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        $logsDir = realpath(__DIR__.'/../var/logs');
        try {
            Common::chmodRecursive($logsDir, 0774);
            Common::chownRecursive($logsDir, 'nginx');
            Common::chgrpRecursive($logsDir, 'syduc');

            $output->writeln('OK');
        } catch (\Exception $ex) {
            $output->writeln('Error while try fix permission for path: '.$logsDir);
        }
    });

/**
 * [Subs Thai - OK, WIN, GAMES] Send daily MT by day. Create cron run 1 time / 1 day
 *
 */
$console
    ->register('subs:mt:game:send-by-day')
    ->setDefinition(array(// new InputOption('some-option', null, InputOption::VALUE_NONE, 'Some help'),
        new InputArgument('keywords', InputArgument::OPTIONAL | InputArgument::IS_ARRAY, 'The keywords to be executed', MO::$GAME_KEYWORDS),
    ))
    ->setDescription('Send MT by day for `Game` service Thai')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        /** @var Logger $logger */
        $logger = $app['subs.logger'];

        //Ensure this command only runs once at a time.
        $lock = new LockHandler('subs:mt:game:send-by-day');
        if (!$lock->lock()) {
            $msg = 'This command: "subs:mt:game:send-by-day" is already running in another process.';
            $output->writeln($msg);
            $logger->noticeSlack($msg);

            return 0;
        }

        /** @var Connection $conn */
        $conn = $app['db'];

        /** @var \App\Util\ContentManager $contentManager */
        $contentManager = $app['subs.content_manager'];
        $keywords = array_map('strtoupper', $input->getArgument('keywords'));

        $condition = ' (telco_id = :telcoAIS or telco_id = :telcoTrueMove) '.
            ' AND (DATEDIFF(:now, last_mt_at) != 0 OR DATEDIFF(:now, last_mt_at) is null) AND DATEDIFF(:now, created_at) != 0 '.
            ' AND keyword IN ("'.implode('","', $keywords).'")';

        $sql = 'SELECT COUNT(phone) FROM user WHERE '.$condition;

        $telcoAIS = Telco::AIS;
        $telcoTrueMove = Telco::TRUEMOVE;
        $now = DateTime::getLocalTime();

        $stmt = $conn->executeQuery(
            $sql,
            [
                'telcoAIS' => $telcoAIS,
                'telcoTrueMove' => $telcoTrueMove,
                'now' => $now,
            ]
        );
        $total = intval($stmt->fetchColumn(0));
        $itemPerPage = 50;

        $count = 0;
        $countError = 0;
        $totalPage = intval(ceil($total / $itemPerPage));

        for ($page = 1; $page <= $totalPage; $page++) {
            $output->writeln('--> Page: '.$page.' -----');

            $sql = 'SELECT * FROM user WHERE '.$condition.' ORDER BY created_at ASC '.' LIMIT '.$itemPerPage;
            $stmt = $conn->executeQuery(
                $sql,
                [
                    'telcoAIS' => $telcoAIS,
                    'telcoTrueMove' => $telcoTrueMove,
                    'now' => $now,
                ]
            );
            $users = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            //Process
            foreach ($users as $user) {
                //Check date diff (ensure)
                if (!empty($user['last_mt_at']) && 0 == DateTime::distance($now, $user['last_mt_at'], true)['days']) {
                    $output->writeln('continue!!');
                    $countError++;
                    continue;
                }

                $output->writeln(sprintf('['.DateTime::getLocalTime().']'.' - Sending daily MT (1 SMS /day) for phone: %s, keyword: %s, telcoId = %s ...', $user['phone'], $user['keyword'], $user['telco_id']));

                $senderServiceId = 'mt_'.strtolower(trim($user['keyword'])).'_sender';
                if (!isset($app[$senderServiceId])) {
                    $output->writeln('--> '.sprintf('Keyword %s is invalid!', $user['keyword']));
                    $countError++;
                    continue;
                }

                $price = MO::$SERVICES[$user['keyword']]['price'];

                /** @var DuplicateMTChecker $checker */
                $checker = $app['duplicate_mt_checker'];

                if ($checker->isSentMT($user['keyword'], $user['phone'], $price, $checker::TYPE_DAILY)) {
                    $output->writeln('--> '.sprintf('Daily MT already sent for phone: %s, keyword: %s', $user['phone'], $user['keyword']));
                    $logger->logMtError(sprintf('Daily MT already sent for phone: %s, keyword: %s', $user['phone'], $user['keyword']), [], true);

                    continue;
                }

                /** @var \App\MT\MTSender $mtSender */
                $mtSender = $app[$senderServiceId];
                $nextLinkInfo = $contentManager->getNextLink($user['keyword'], $user['last_link_id']);
                $text = $nextLinkInfo['link'];
                $newLinkId = $nextLinkInfo['id'];

                if (sendMT($mtSender, $conn, $logger, $app['gg_analytics'], $app['subs_api_receive'], $app['mt_channel'], $user, $text, $newLinkId)) {
                    $count++;
                    $output->writeln('--> Done.');
                } else {
                    $output->writeln('--> Error.');
                    $countError;
                }
            } //end foreach $users
        } //end for $page

        //Close connection
        if (false !== $conn->ping()) {
            $conn->close();
        }

        $lock->release(); //Unlock process
        $output->writeln(sprintf('Total: %d', $count));
        $output->writeln('Error: '.$countError);
        $output->writeln('Done');

        return true;
    });

/**
 * [Subs Thai - OK, WIN, GAMES] Send daily MT by day for telco DTAC. Create cron run 1 time / 1 day
 *
 */
$console
    ->register('subs:mt:game:dtac:send-by-day')
    ->setDefinition(array(// new InputOption('some-option', null, InputOption::VALUE_NONE, 'Some help'),
        new InputArgument('keywords', InputArgument::OPTIONAL | InputArgument::IS_ARRAY, 'The keywords to be executed', MO::$GAME_KEYWORDS),
    ))
    ->setDescription('Send MT by day for `Game` service Thai - telco `DTAC` - keyword `OK`')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        /** @var Connection $conn */
        $conn = $app['db'];

        /** @var Logger $logger */
        $logger = $app['subs.logger'];

        /** @var \App\Util\ContentManager $contentManager */
        $contentManager = $app['subs.content_manager'];

        //send by each keyword
        $keywords = array_map('strtoupper', $input->getArgument('keywords'));
        $telcoDTAC = Telco::DTAC;
        $now = DateTime::getLocalTime();

        /** @var DuplicateMTChecker $checker */
        $checker = $app['duplicate_mt_checker'];

        $total = [];
        foreach ($keywords as $keyword) {
            $total[$keyword] = 0;

            //Check MT is sent today for this keyword
            $sql = 'select id from mt_dtac where keyword = :keyword and created_date = :date';
            $stmt = $conn->executeQuery($sql, ['keyword' => $keyword, 'date' => DateTime::getLocalTime('Y-m-d')]);

            if (empty($stmt->fetchColumn(0))) {
                //Check duplicate mt sent
                $price = MO::$SERVICES[$keyword]['price'];

                if ($checker->isSentMT($keyword, 1, $price, $checker::TYPE_DAILY)) {
                    $output->writeln('--> '.sprintf('Daily MT telco DTAC already sent for phone: %s, keyword: %s', 1, $keyword));
                    $logger->logMtError(sprintf('Daily MT telco DTAC already sent for phone: %s, keyword: %s', 1, $keyword), [], true);

                    continue;
                }

                $sql = 'SELECT * FROM user WHERE telco_id = :telcoDTAC '.
                    ' AND (DATEDIFF(:now, last_mt_at) is null or DATEDIFF(:now, last_mt_at) != 0) '.
                    ' AND keyword = :keyword ORDER BY created_at ASC LIMIT 1';

                try {
                    $stmt = $conn->prepare($sql);
                    $stmt->bindParam('keyword', $keyword);
                    $stmt->bindParam('telcoDTAC', $telcoDTAC);
                    $stmt->bindParam('now', $now);

                    $stmt->execute();
                    $user = $stmt->fetch(\PDO::FETCH_ASSOC);
                } catch (\Exception $ex) {
                    $output->writeln(sprintf('--> Somethings went wrong. %s', $ex->getMessage()));
                    continue;
                }

                if (!empty($user)) {
                    $lastLinkId = $user['last_link_id'];
                    $nextLinkInfo = $contentManager->getNextLink($keyword, $lastLinkId);
                    $text = $nextLinkInfo['link'];
                    $newLinkId = $nextLinkInfo['id'];
                    $to = 1; //send to all phones subscribed of telco DTAC
                    $telcoId = $user['telco_id'];
                    $moId = '1'; //fake, DTAC no need moid

                    $output->writeln(sprintf('['.DateTime::getLocalTime().']'.' - Sending daily MT (1 SMS /day) for telco DTAC, phone: %s, keyword: %s, telcoId = %s ...', $to, $keyword, $telcoId));

                    $senderServiceId = 'mt_'.strtolower(trim($keyword)).'_sender';
                    if (!isset($app[$senderServiceId])) {
                        $output->writeln('--> '.sprintf('Keyword %s is invalid!', $keyword));
                        continue;
                    }

                    /** @var \App\MT\MTSender $mtSender */
                    $mtSender = $app[$senderServiceId];

                    $total[$keyword] = sendMTDTAC($mtSender, $conn, $logger, $app['gg_analytics'], $keyword, $telcoId, $moId, $text, $newLinkId);

                } //end check if(!empty($user))
            }
        } //end foreach keywords

        //Close connection
        if (false !== $conn->ping()) {
            $conn->close();
        }

        //Summary message
        $output->writeln('Total: ');
        foreach ($total as $keyword => $count) {
            $output->writeln(sprintf('-->  %s: %d', $keyword, $count));
        }

        return true;
    });

/**
 * Re-send MT, use in case MT response is fail
 */
$console
    ->register('subs:mt:game:resend')
    ->setDefinition(array(// new InputOption('some-option', null, InputOption::VALUE_NONE, 'Some help'),
        new InputArgument('keywords', InputArgument::OPTIONAL | InputArgument::IS_ARRAY, 'The keywords to be executed', MO::$GAME_KEYWORDS),
    ))
    ->setDescription('Re-send mt')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        /** @var Connection $conn */
        $conn = $app['db'];

        /** @var Logger $logger */
        $logger = $app['subs.logger'];

        /** @var \App\Util\ContentManager $contentManager */
        $contentManager = $app['subs.content_manager'];

        /** @var GoogleAnalytics $ga */
        $ga = $app['gg_analytics'];

        $keywords = array_map('strtoupper', $input->getArgument('keywords'));
        $condition = ' telco_id != :telcoDTAC AND DATE(last_mt_at) = :date  AND keyword IN ("'.implode('","', $keywords).'") '.
            ' AND (last_mt_response is null or LOWER(last_mt_response) = "null" or (LENGTH(last_mt_response) - LENGTH(REPLACE(last_mt_response, ",", ""))) < 2 OR SUBSTRING(last_mt_response, -3) != "200") ';
        $sql = 'SELECT COUNT(phone) FROM user WHERE '.$condition;

        $stmt = $conn->executeQuery($sql, ['telcoDTAC' => Telco::DTAC, 'date' => DateTime::getLocalTime('Y-m-d')]);
        $total = intval($stmt->fetchColumn(0));

        $itemPerPage = 50;
        $count = 0;
        $countError = 0;
        $totalPage = intval(ceil($total / $itemPerPage));
        for ($page = 1; $page <= $totalPage; $page++) {
            $sql = 'SELECT * FROM user WHERE '.$condition.' ORDER BY last_mt_at ASC '.' LIMIT '.$itemPerPage;
            $stmt = $conn->executeQuery($sql, ['telcoDTAC' => Telco::DTAC, 'date' => DateTime::getLocalTime('Y-m-d')]);
            $users = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            foreach ($users as $user) {
                $output->writeln(sprintf('['.DateTime::getLocalTime().']'.' - Sending daily MT (1 SMS /day) for phone: %s, keyword: %s, telcoId = %s ...', $user['phone'], $user['keyword'], $user['telco_id']));

                $senderServiceId = 'mt_'.strtolower(trim($user['keyword'])).'_sender';
                if (!isset($app[$senderServiceId])) {
                    $output->writeln('--> '.sprintf('Keyword %s is invalid!', $user['keyword']));
                    continue;
                }

                /** @var \App\MT\MTSender $mtSender */
                $mtSender = $app[$senderServiceId];

                //Get current link info (don't get next link info)
                $nextLinkInfo = $contentManager->getLinkInfo($user['keyword'], $user['last_link_id']);
                $text = $nextLinkInfo['link'];
                $newLinkId = $nextLinkInfo['id'];

                if (sendMT($mtSender, $conn, $logger, $ga, $app['subs_api_receive'], $app['mt_channel'], $user, $text, $newLinkId)) {
                    $count++;
                    $output->writeln('--> Done.');
                } else {
                    $output->writeln('--> Error.');
                    $countError++;
                }
            } //-> end foreach $users
        } //-> end foreach $pages

        //Close connection
        if (false !== $conn->ping()) {
            $conn->close();
        }

        $output->writeln('Result:');
        $output->writeln('-> Re-sent MT success: '.$count);
        $output->writeln('-> Re-sent MT fail: '.$countError);
    });

/**
 * Re-send MT for telco DTAC, use in case MT response is fail
 */
$console
    ->register('subs:mt:game:dtac:resend')
    ->setDefinition(array(// new InputOption('some-option', null, InputOption::VALUE_NONE, 'Some help'),
        new InputArgument('keywords', InputArgument::OPTIONAL | InputArgument::IS_ARRAY, 'The keywords to be executed', MO::$GAME_KEYWORDS),
    ))
    ->setDescription('Re-send mt for telco DTAC')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        /** @var Connection $conn */
        $conn = $app['db'];

        /** @var Logger $logger */
        $logger = $app['subs.logger'];

        /** @var \App\Util\ContentManager $contentManager */
        $contentManager = $app['subs.content_manager'];

        $total = [];
        //send by each keyword
        $keywords = array_map('strtoupper', $input->getArgument('keywords'));
        foreach ($keywords as $keyword) {
            $total[$keyword] = 0;

            $sql = 'SELECT * FROM user WHERE telco_id = :telcoDTAC AND keyword = :keyword '.
                ' AND DATE(last_mt_at) = :date '.
                ' AND (last_mt_response is null or LOWER(last_mt_response) = "null" or (LENGTH(last_mt_response) - LENGTH(REPLACE(last_mt_response, ",", ""))) < 2 OR SUBSTRING(last_mt_response, -3) != "200") '.
                ' ORDER BY created_at ASC LIMIT 1 ';

            try {
                $stmt = $conn->executeQuery(
                    $sql,
                    [
                        'keyword' => $keyword,
                        'telcoDTAC' => Telco::DTAC,
                        'date' => DateTime::getLocalTime('Y-m-d'),
                    ]
                );

                $user = $stmt->fetch(\PDO::FETCH_ASSOC);

            } catch (\Exception $ex) {
                $output->writeln(sprintf('--> Somethings went wrong. %s', $ex->getMessage()));
                continue;
            }

            if (!empty($user)) {
                $lastLinkId = $user['last_link_id'];
                $nextLinkInfo = $contentManager->getLinkInfo($keyword, $lastLinkId);
                $text = $nextLinkInfo['link'];
                $newLinkId = $nextLinkInfo['id'];
                $to = 1; //send to all phones subscribed of telco DTAC
                $telcoId = $user['telco_id'];
                $moId = '1'; //fake, DTAC no need moid

                $output->writeln(sprintf('['.DateTime::getLocalTime().']'.' - Sending daily MT (1 SMS /day) for telco DTAC, phone: %s, keyword: %s, telcoId = %s ...', $to, $keyword, $telcoId));

                $senderServiceId = 'mt_'.strtolower(trim($keyword)).'_sender';
                if (!isset($app[$senderServiceId])) {
                    $output->writeln('--> '.sprintf('Keyword %s is invalid!', $keyword));
                    continue;
                }

                /** @var \App\MT\MTSender $mtSender */
                $mtSender = $app[$senderServiceId];

                $total[$keyword] = sendMTDTAC($mtSender, $conn, $logger, $app['gg_analytics'], $keyword, $telcoId, $moId, $text, $newLinkId);

            } //end check if(!empty($user))
        } //end foreach keywords

        //Close connection
        if (false !== $conn->ping()) {
            $conn->close();
        }

        //Summary message
        $output->writeln('Total: ');
        foreach ($total as $keyword => $count) {
            $output->writeln(sprintf('-->  %s: %d', $keyword, $count));
        }

    });

/**
 * Check out of link, notify to Slack when link is limited
 */
$console
    ->register('subs:link:check')
    ->setDefinition(array(// new InputOption('some-option', null, InputOption::VALUE_NONE, 'Some help'),
        new InputArgument('keywords', InputArgument::OPTIONAL | InputArgument::IS_ARRAY, 'The keywords to be executed', MO::$GAME_KEYWORDS),
    ))
    ->setDescription('Check out of link')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        /** @var Connection $conn */
        $conn = $app['db'];
        /** @var Logger $logger */
        $logger = $app['subs.logger'];
        $keywords = array_map('strtoupper', $input->getArgument('keywords'));

        //Get current link id
        $sql = 'SELECT keyword, MAX(CAST(last_link_id as SIGNED INTEGER)) AS current FROM user WHERE keyword IN (:keywords) GROUP BY keyword';
        $stmt = $conn->executeQuery(
            $sql,
            [
                'keywords' => $keywords,
            ],
            [
                'keywords' => Connection::PARAM_STR_ARRAY,
            ]
        );
        $current = $stmt->fetchAll();
        $currentArr = [];
        foreach ($current as $item) {
            $currentArr[$item['keyword']] = intval($item['current']);
        }

        //Check
        foreach ($currentArr as $keyword => $currentId) {
            $sql = 'SELECT count(id) AS count FROM link WHERE id > :id AND keyword = :keyword';
            $stmt = $conn->executeQuery($sql, ['id' => $currentId, 'keyword' => $keyword]);
            $count = intval($stmt->fetchColumn(0));
            if (5 >= $count) {
                //notify to Slack
                $specialLogger = clone $logger;
                $specialLogger->setSlackChannel($app['slack.chanel_special']);

                $msg = sprintf(
                    'Chỉ còn %s links content cho keyword "%s". Vào %s để add thêm.',
                    $count, $keyword, $app['subs_api_content_manager']
                );
                $specialLogger->noticeSlack($msg);
            }
        }

        print_r($currentArr);
    });

/**
 * Call api tracking
 */
$console
    ->register('subs:aff:send')
    ->setDefinition(array(// new InputOption('some-option', null, InputOption::VALUE_NONE, 'Some help'),
        new InputArgument('keywords', InputArgument::OPTIONAL | InputArgument::IS_ARRAY, 'The keywords to be executed', MO::$GAME_KEYWORDS),
    ))
    ->setDescription('Call api tracking')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        /** @var Connection $conn */
        $conn = $app['db'];

        /** @var Logger $logger */
        $logger = $app['subs.logger'];
        $keywords = array_map('strtoupper', $input->getArgument('keywords'));

        $sql = 'select * from aff where status = 1 and (sent = 0 or sent is null) and keyword in (:keywords)';
        $stmt = $conn->executeQuery(
            $sql,
            [
                'keywords' => $keywords,
            ],
            [
                'keywords' => Connection::PARAM_STR_ARRAY,
            ]
        );
        $items = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $client = new \GuzzleHttp\Client();

        $logAffPath = dirname(__DIR__).'/var/logs/aff';
        if (!is_dir($logAffPath)) {
            mkdir($logAffPath, 0774, true);
        }
        $logFile = date('Y-m-d').'_aff_track.txt';

        $phones = [];
        foreach ($items as $item) {
            $output->writeln(sprintf('- Processing item click_id: %s, phone: %s ...', $item['click_id'], $item['phone']));

            $url = sprintf(MO::AFF_TRACK_API.'?click_id=%s&adv=%s', $item['click_id'], MO::AFF_TRACK_USER);
            try {
                $response = $client->get($url)->getBody()->getContents();
            } catch (\Exception $ex) {
                $response = null;
                $logger->noticeSlack('Caught error when call api tracking. Error: '.$ex->getMessage());
            }
            $output->writeln('==> Response: '.$response);

            //Log
            $logData = [
                'phone' => $item['phone'],
                'click_id' => $item['click_id'],
                'keyword' => $item['keyword'],
                'pub' => $item['pub'],
                'response' => $response,
            ];
            file_put_contents($logAffPath.'/'.$logFile, '['.DateTime::getLocalTime().'] '.json_encode($logData).PHP_EOL, FILE_APPEND);

            $phones[] = $item['phone'];
            try {
                $conn->update(
                    'aff',
                    [
                        'sent' => 1,
                        'response' => $response,
                        'last_updated_at' => DateTime::getLocalTime(),
                    ],
                    [
                        'phone' => $item['phone'],
                    ]
                );

                //Insert table `aff_track`
                $conn->insert(
                    'aff_track',
                    [
                        'phone' => $item['phone'],
                        'click_id' => $item['click_id'],
                        'telco_id' => $item['telco_id'],
                        'response' => $response,
                        'pub' => $item['pub'],
                        'keyword' => $item['keyword'],
                        'created_at' => DateTime::getLocalTime(),
                        'created_date' => DateTime::getLocalTime('Y-m-d'),
                    ]
                );
            } catch (\Exception $ex) {
                $logger->noticeSlack('Caught error when update aff info. Error: '.$ex->getMessage());
            }
        }

        //Delete phones sent successful
        $conn->createQueryBuilder()
            ->delete('aff')
            ->where('phone in (:phones)')
            ->setParameter(':phones', $phones, Connection::PARAM_STR_ARRAY)
            ->execute();

        $output->writeln('Done.');
        $output->writeln('Count: '.count($items));
    });

/**
 * Check diff mt between today and yesterday
 *
 * Eg. php bin/console subs:mt:check-diff OK
 */
$console
    ->register('subs:mt:check-diff')
    ->setDefinition(array(// new InputOption('some-option', null, InputOption::VALUE_NONE, 'Some help'),
        new InputArgument('keyword', InputArgument::REQUIRED, 'The keyword to be executed'),
    ))
    ->setDescription('Check diff mt daily between today and yesterday')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        /** @var Connection $conn */
        $conn = $app['db'];

        /** @var Logger $logger */
        $logger = $app['subs.logger'];

        $specialLogger = clone $logger;
        $specialLogger->setSlackChannel($app['slack.chanel_special']);

        $keyword = strtoupper($input->getArgument('keyword'));

        $sql = 'select created_date, count(id) as count'.
            ' from mt where keyword = :keyword and (created_date between SUBDATE(CURDATE(),1) and CURDATE()) and is_welcome = 0 group by created_date';

        $stmt = $conn->executeQuery($sql, ['keyword' => $keyword]);
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $counts = array_column($data, 'count');

        if (1 == count($counts)) {
            $specialLogger->noticeSlack(
                sprintf('Có bất thường khi check diff MT cho keyword %s', $keyword),
                $data
            );
        }

        if (2 == count($counts) && (MT::LIMIT_CHECK_DIFF < abs($counts[1] - $counts[0]))) {
            $specialLogger->noticeSlack(
                sprintf('Lệch MT giữa hôm qua và hôm nay quá nhiều, keyword %s', $keyword),
                $data
            );
        }

        $output->writeln('Done.');
    });

/**
 * Post MO info to aff tracking
 */
$console
    ->register('subs:aff:post-mo')
    ->setDefinition([// new InputOption('some-option', null, InputOption::VALUE_NONE, 'Some help'),
        new InputOption('limit', 'l', InputOption::VALUE_OPTIONAL, 'Count of items will be executed', 100),
    ])
    ->setDescription('Post MO info to aff tracking')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        /** @var Connection $conn */
        $conn = $app['db'];

        /** @var Logger $logger */
        $logger = $app['subs.logger'];
        $limit = intval($input->getOption('limit'));

        $sql = 'select mo_id, phone, keyword, shortcode, time_utc, telco_id, is_register, channel '.
            ' from mo where is_send_track = 0 limit '.$limit;

        $stmt = $conn->executeQuery($sql);
        $items = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $ids = array_column($items, 'mo_id');

        /** @var \GuzzleHttp\Client $client */
        $client = $app['subs.http_client'];

        if (!empty($items)) {
            $logger->logMo('Send MO track', ['url' => MO::MO_TRACK_API, 'data' => $items]);

            try {
                $response = $client->post(
                    MO::MO_TRACK_API,
                    [
                        'form_params' => [
                            'token' => MO::MO_TRACK_API_TOKEN,
                            'data' => $items,
                        ],
                    ]
                );

                $isSendSuccess = (200 == $response->getStatusCode()) ? true : false;

            } catch (\Exception $ex) {
                $isSendSuccess = false;
                $logger->logMoError('Send MO track error', ['url' => MO::MO_TRACK_API, 'data' => $items, 'statusCode' => $ex->getCode()]);

                $logger->noticeSlack(
                    sprintf('Caught error while post MO info to api "%s", status code "%s"; Error %s ',
                        MO::MO_TRACK_API, $ex->getCode(), $ex->getMessage()),
                    [
                        'data' => $items,
                    ]
                );
            }

            $sql = 'update mo set is_send_track = 1, is_send_track_success = :isSuccess where mo_id in (:ids)';
            $conn->executeUpdate(
                $sql,
                [
                    'isSuccess' => $isSendSuccess,
                    'ids' => $ids,
                ],
                [
                    'isSuccess' => \PDO::PARAM_BOOL,
                    'ids' => Connection::PARAM_STR_ARRAY,
                ]
            );
        }

        $output->writeln('Done');
    });

/**
 * Send MT retry
 */
$console
    ->register('subs:mt:retry')
    ->setDefinition([// new InputOption('some-option', null, InputOption::VALUE_NONE, 'Some help'),
        new InputArgument('at', InputArgument::REQUIRED, 'Sent retry at AM or PM'),
        new InputOption('limit', 'l', InputOption::VALUE_OPTIONAL, 'Count of items will be executed', 500),
    ])
    ->setDescription('Retry send MT')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        $at = $input->getArgument('at');
        if (!in_array($at, ['am', 'pm'])) {
            $output->writeln('Invalid argument. Available values: am, pm');
            exit;
        }

        $limit = intval($input->getOption('limit'));
        if ($limit <= 0) {
            $limit = 500;
        }

        /** @var Connection $conn */
        $conn = $app['db'];

        /** @var Logger $logger */
        $logger = $app['subs.logger'];

        /** @var \App\Util\ContentManager $contentManager */
        $contentManager = $app['subs.content_manager'];

        $timeMTStart = '';
        $timeMTEnd = '';

        //Process with timezone GMT+7
        $date = DateTime::setTimezone('now', 'GMT+7', 'Y-m-d');
        $current = DateTime::setTimezone('now', 'GMT+7');

        if ('am' == $at) {
            //check current time in range (2017-07-07 08:00:00 - 2017-07-07 12:00:00)
            $min = $date.' 08:00:00';
            $max = $date.' 12:00:00';

            if ($current > $min && $current < $max) {
                $timeMTStart = DateTime::dateSub($current, 1, 'Y-m-d').' 12:01:00';
                $timeMTEnd = DateTime::dateSub($current, 1, 'Y-m-d').' 19:00:00';
            }

        } else { //pm
            //check current time in range (2017-07-07 12:02:00 - 2017-07-07 19:00:00)
            $min = $date.' 12:02:00';
            $max = $date.' 19:00:00';

            if ($current > $min && $current < $max) {
                $timeMTStart = $date.' 08:00:00';
                $timeMTEnd = $date.' 12:00:00';
            }
        }

        if (empty($timeMTStart) || empty($timeMTEnd)) {
            $output->writeln('Nothing to processing :)');

            return true;
        }

        $timeMTStartUtc = DateTime::setTimezone($timeMTStart, 'UTC');
        $timeMTEndUtc = DateTime::setTimezone($timeMTEnd, 'UTC');

        $sql = 'select u.*, mr.id as mt_retry_id '
            .' from user as u inner join mt_retry as mr on u.phone = mr.phone '
            .' where u.telco_id = :telcoId and mr.retry = 0 and u.has_first_mt = 1 '
            .' and (time_to_sec(timediff(u.last_mt_at, u.created_at))/60 > 30) ' //avoid first mt
            .' and (u.last_mt_at_utc between :timeStart and :timeEnd) and u.is_retry_today = 0 limit '.$limit;

        $stmt = $conn->executeQuery(
            $sql,
            [
                'telcoId' => Telco::AIS,
                'timeStart' => $timeMTStartUtc,
                'timeEnd' => $timeMTEndUtc,
            ]
        );

        $users = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $countError = 0;
        $count = 0;

        foreach ($users as $user) {
            //send here
            $output->writeln(sprintf('['.DateTime::getLocalTime().']'.' - Sending daily MT retry for phone: %s, keyword: %s, telcoId = %s ...', $user['phone'], $user['keyword'], $user['telco_id']));

            $senderServiceId = 'mt_'.strtolower(trim($user['keyword'])).'_sender';
            if (!isset($app[$senderServiceId])) {
                $output->writeln('--> '.sprintf('Keyword %s is invalid!', $user['keyword']));
                $countError++;
                continue;
            }

            /** @var \App\MT\MTSender $mtSender */
            $mtSender = $app[$senderServiceId];

            //Get current link info (don't get next link info)
            $nextLinkInfo = $contentManager->getLinkInfo($user['keyword'], $user['last_link_id']);

            $text = $nextLinkInfo['link'];
            $newLinkId = $nextLinkInfo['id'];

            if (
            sendMT(
                $mtSender,
                $conn,
                $logger,
                $app['gg_analytics'],
                $app['subs_api_receive'],
                $app['mt_channel'],
                $user,
                $text,
                $newLinkId,
                MT::MT_TYPE_RETRY,
                false)
            ) {
                $count++;
                $output->writeln('--> Done.');
            } else {
                $output->writeln('--> Error.');
                $countError;
            }

            $conn->update(
                'mt_retry',
                [
                    'retry' => 1,
                ],
                [
                    'id' => $user['mt_retry_id'],
                ]
            );
        }

    });

/**
 * Reset status `is_retry_status`
 */
$console
    ->register('subs:mt:retry:reset-status')
    ->setDefinition(array(// new InputOption('some-option', null, InputOption::VALUE_NONE, 'Some help'),
    ))
    ->setDescription('Reset field `is_retry_today` for user in end day')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        /** @var Connection $conn */
        $conn = $app['db'];

        $sql = 'update user set is_retry_today = 0 where is_retry_today = 1';
        $conn->executeUpdate($sql);

        $output->writeln('Reset field `is_retry_today` successful');
    });

/**
 * Update keyword for table `mt_dn`
 */
$console
    ->register('subs:dn:update-keyword')
    ->setDefinition(array(// new InputOption('some-option', null, InputOption::VALUE_NONE, 'Some help'),
    ))
    ->setDescription('Update keyword for table mt_dn')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        /** @var Connection $conn */
        $conn = $app['db'];

        $keywords = MO::$GAME_KEYWORDS;
        foreach ($keywords as $keyword) {
            $sql = 'select DISTINCT mt_dn.phone from mo inner join mt_dn on mt_dn.phone = mo.phone where mo.keyword = "'.$keyword.'";';
            $stmt = $conn->executeQuery($sql);
            $phones = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            foreach ($phones as $phone) {
                $sql = 'update mt_dn set keyword="'.$keyword.'" where phone = "'.$phone['phone'].'" and keyword is null';
                $stmt = $conn->executeUpdate($sql);
                $output->writeln('Update for phone: '.$phone['phone'].', keyword: '.$keyword);
            }
        }
        $output->writeln('Done.');
    });

/**
 * Insert a record to table `mt`
 *
 * @param Connection $conn
 * @param string     $phone
 * @param string     $from
 * @param string     $telcoId
 * @param string     $content
 * @param string     $keyword
 * @param null       $type
 * @param null       $status
 * @param null       $mtResponse
 *
 * @return int
 */
function insertMT(Connection $conn, $phone, $from, $telcoId, $content, $keyword, $type = null, $status = null, $mtResponse = null)
{
    //Check mysql timeout
    if (false === $conn->ping()) {
        $conn->close();
        $conn->connect();
    }

    $result = $conn->insert(
        'mt',
        [
            'phone' => $phone,
            '`from`' => $from,
            'telco_id' => $telcoId,
            'content' => $content,
            'keyword' => $keyword,
            'type' => $type,
            'is_welcome' => (MT::MT_TYPE_WELCOME == $type) ? 1 : 0,
            'status' => $status,
            'is_success' => (MT::MT_STATUS_SUCCESS == $status) ? 1 : 0,
            'created_at' => DateTime::getLocalTime(),
            'time_utc' => DateTime::getUTCTime(),
            'created_date' => DateTime::addHourLocalTime(DateTime::getLocalTime(), 1, 'Y-m-d'),
            'last_mt_response' => $mtResponse,
        ]
    );

    //Close connection
    if (false !== $conn->ping()) {
        $conn->close();
    }

    return $result;
}

/**
 * Insert a record to table `mt_dtac`
 *
 * @param Connection $conn
 * @param string     $phone
 * @param string     $from
 * @param string     $telcoId
 * @param string     $content
 * @param string     $keyword
 * @param null       $type
 * @param null       $status
 * @param int        $count
 * @param null       $mtResponse
 *
 * @return int
 */
function insertMTDTAC(Connection $conn, $phone, $from, $telcoId, $content, $keyword, $type = null, $status = null, $count = 0, $mtResponse = null)
{
    //Check mysql timeout
    if (false === $conn->ping()) {
        $conn->close();
        $conn->connect();
    }

    $result = $conn->insert(
        'mt_dtac',
        [
            'phone' => $phone,
            '`from`' => $from,
            'telco_id' => $telcoId,
            'content' => $content,
            'keyword' => $keyword,
            'type' => $type,
            'is_welcome' => (MT::MT_TYPE_WELCOME == $type) ? 1 : 0,
            'status' => $status,
            'is_success' => (MT::MT_STATUS_SUCCESS == $status) ? 1 : 0,
            'created_at' => DateTime::getLocalTime(),
            'time_utc' => DateTime::getUTCTime(),
            'count' => $count,
            'created_date' => DateTime::addHourLocalTime(DateTime::getLocalTime(), 1, 'Y-m-d'),
            'last_mt_response' => $mtResponse,
        ]
    );

    //Close connection
    if (false !== $conn->ping()) {
        $conn->close();
    }

    return $result;
}

/**
 * Try to send request to MK api, retry 3 times when response is '500'
 *
 * @param MTSender $mtSender
 * @param string   $to
 * @param string   $text
 * @param string   $keyword
 * @param string   $telcoId
 * @param string   $cat
 * @param          $type
 * @param          $price
 * @param          $moId
 * @param bool     $isWelcome
 * @param bool     $isForceRetry
 * @param array    $result
 * @param bool     $isSuccess
 * @param int      $retry
 *
 * @return array
 */
function trySendMT(
    MTSender $mtSender,
    $to,
    $text,
    $keyword,
    $telcoId,
    $cat,
    $type,
    $price,
    $moId,
    $isWelcome = false,
    $isForceRetry = false,
    $result = null,
    $isSuccess = false,
    $retry = 0
) {
    if (MTSender::MAX_RETRY == $retry - 1) {
        return [
            'params' => $mtSender->getParams(),
            'url' => $mtSender->getUrl(),
            'isSuccess' => $isSuccess,
            'result' => $result,
            'retry' => $retry - 1,
        ];
    }

    try {
        $result = $mtSender->send($to, $text, $keyword, $telcoId, $cat, $type, $price, $moId, $isWelcome);
        $parts = explode(',', $result);
        if (('500' == $result || is_null($result) || 'null' == $result || is_null($result) || 3 !== count($parts)) && $isForceRetry) {
            sleep(3);
            $retry += 1;

            return trySendMT($mtSender, $to, $text, $keyword, $telcoId, $cat, $type, $price, $moId, $isWelcome, $isForceRetry, $result, $isSuccess, $retry);
        }

        if (3 === count($parts)) {
            $isSuccess = true;
        }
    } catch (\Exception $ex) {
        $isSuccess = false;
    }

    return [
        'params' => $mtSender->getParams(),
        'url' => $mtSender->getUrl(),
        'isSuccess' => $isSuccess,
        'result' => $result,
        'retry' => $retry,
    ];
}

/**
 * Make a request send MO to our system
 *
 * @param $api
 * @param $from
 * @param $text
 * @param $time
 * @param $shortCode
 * @param $moId
 * @param $telcoId
 * @param $channel
 *
 * @return string
 */
function sendMO($api, $from, $text, $time, $shortCode, $moId, $telcoId, $channel)
{
    $client = new \GuzzleHttp\Client();
    $params = [
        'from' => $from,
        'text' => $text,
        'time' => $time,
        'shortcode' => $shortCode,
        'telcoid' => $telcoId,
        'moid' => $moId,
        'channel' => $channel,
        'source' => 'local',
    ];

    $url = $api.'?'.Common::buildQueryString($params);

    return $client->get($url)->getBody()->getContents();
}

/**
 * Send MT
 *
 * @param MTSender        $MTSender
 * @param Connection      $conn
 * @param Logger          $logger
 * @param GoogleAnalytics $ga
 * @param                 $apiReceiveMO
 * @param                 $channel
 * @param                 $user
 * @param                 $text
 * @param                 $newLinkId
 * @param string          $type
 * @param bool            $isForceRetry
 *
 * @return bool
 */
function sendMT(
    MTSender $MTSender,
    Connection $conn,
    Logger $logger,
    GoogleAnalytics $ga,
    $apiReceiveMO,
    $channel,
    $user,
    $text,
    $newLinkId,
    $type = MT::MT_TYPE_DAILY,
    $isForceRetry = false
) {
    return doSendMT(
        $MTSender,
        $conn,
        $logger,
        $ga,
        $apiReceiveMO,
        $channel,
        $user['phone'],
        $user['keyword'],
        $user['telco_id'],
        $user['mo_id'],
        $text,
        $newLinkId,
        $type,
        $isForceRetry
    );
}

/**
 * @param MTSender        $mtSender
 * @param Connection      $conn
 * @param Logger          $logger
 * @param GoogleAnalytics $ga
 * @param                 $apiReceiveMO
 * @param                 $channel
 * @param                 $phone
 * @param                 $keyword
 * @param                 $telcoId
 * @param                 $moId
 * @param                 $text
 * @param                 $newLinkId
 * @param                 $type
 * @param bool            $isForceRetry
 *
 * @return bool
 */
function doSendMT(
    MTSender $mtSender,
    Connection $conn,
    Logger $logger,
    GoogleAnalytics $ga,
    $apiReceiveMO,
    $channel,
    $phone,
    $keyword,
    $telcoId,
    $moId,
    $text,
    $newLinkId,
    $type,
    $isForceRetry
) {
    $shortCode = MO::$SERVICES[$keyword]['shortcode'];
    $price = MO::$SERVICES[$keyword]['price'];

    $dataResult = trySendMT(
        $mtSender,
        $phone,
        $text,
        $keyword,
        $telcoId,
        MT::getCatDailyMT($telcoId),
        MT::getTypeDailyMT($telcoId),
        $price,
        $moId,
        false,
        $isForceRetry
    ); //Force retry send MT 3 times if error

    $isSuccess = $dataResult['isSuccess'];
    $retry = $dataResult['retry'];
    $result = $dataResult['result'];
    $paramsSent = $dataResult['params'];

    $logData = [
        'result' => $result,
        'retry' => $retry,
        'url' => $dataResult['url'],
    ];

    //Make a request MO unregistered if response = 410 (Invalid Subscriber)
    if ('410' == $result || '406' == $result) {
        $dateTime = DateTime::getLocalTime('Y-m-dH:i:s');
        //sendMO($apiReceiveMO, $phone, 'STOP '.$keyword, $dateTime, $shortCode, time(), $telcoId, $channel);

        //return false;
    }

    $prefixGaLabel = (MT::MT_TYPE_RETRY == $type) ? 'retry__' : '';

    //GA
    if ($isSuccess) {
        $action = GoogleAnalytics::MSG_MT_SEND_SUCCESS;
        $gaLabel = $prefixGaLabel.$result.'__'.$phone;
    } else {
        $action = GoogleAnalytics::MSG_MT_SEND_FAIL;
        $gaLabel = $prefixGaLabel.$result.'__'.json_encode($paramsSent);
    }
    $ga->sendEventMT($action, $gaLabel);

    //Check mysql timeout
    if (false === $conn->ping()) {
        $conn->close();
        $conn->connect();
    }

    if ($isSuccess) {
        $logger->logMtSuccess('Send '.$type.' MT success', $logData);
    } else {
        $logger->logMtError('Send '.$type.' MT error!', $logData, true);
    }

    //Insert MT
    $status = $isSuccess ? MT::MT_STATUS_SUCCESS : MT::MT_STATUS_FAIL;
    try {

        //Update last_mt time for user
        if (MT::MT_TYPE_RETRY != $type) {
            $conn->update(
                'user',
                [
                    'last_link_id' => $newLinkId,
                    'last_mt_at' => DateTime::getLocalTime(),
                    'last_mt_at_utc' => DateTime::getUTCTime(),
                    'last_mt_response' => $result,
                ],
                [
                    'phone' => $phone,
                ]
            );
        }

        if (MT::MT_TYPE_RETRY == $type) {
            $conn->update(
                'user',
                [
                    'is_retry_today' => 1,
                ],
                [
                    'phone' => $phone,
                ]
            );
        }

        insertMT($conn, $phone, $shortCode, $telcoId, $text, $keyword, $type, $status, $result);

    } catch (\Exception $ex) {
        $logger->logMtError('Insert a record into table `mt` fail, because '.$ex->getMessage(), $logData, true);
    }

    return $isSuccess;
}

/**
 * Send MT for telco DTAC
 *
 * @param MTSender        $mtSender
 * @param Connection      $conn
 * @param Logger          $logger
 * @param GoogleAnalytics $ga
 * @param                 $keyword
 * @param                 $telcoId
 * @param                 $moId
 * @param                 $text
 * @param                 $newLinkId
 *
 * @return int
 */
function sendMTDTAC(
    MTSender $mtSender,
    Connection $conn,
    Logger $logger,
    GoogleAnalytics $ga,
    $keyword,
    $telcoId,
    $moId,
    $text,
    $newLinkId
) {
    $shortCode = MO::$SERVICES[$keyword]['shortcode'];
    $price = MO::$SERVICES[$keyword]['price'];
    $phone = 1; //send to all phones subscribed of telco DTAC

    //Force retry send MT 3 times if error
    $dataResult = trySendMT(
        $mtSender,
        $phone,
        $text,
        $keyword,
        $telcoId,
        MT::getCatDailyMT($telcoId),
        MT::getTypeDailyMT($telcoId),
        $price,
        $moId,
        false,
        true
    );

    $isSuccess = $dataResult['isSuccess'];
    $retry = $dataResult['retry'];
    $result = $dataResult['result'];
    $paramsSent = $dataResult['params'];

    $logData = [
        'result' => $result,
        'retry' => $retry,
        'url' => $dataResult['url'],
    ];

    //GA
    if ($isSuccess) {
        $action = GoogleAnalytics::MSG_MT_SEND_SUCCESS;
        $gaLabel = $result.'__'.$phone;
    } else {
        $action = GoogleAnalytics::MSG_MT_SEND_FAIL;
        $gaLabel = $result.'__'.json_encode($paramsSent);
    }
    $ga->sendEventMT($action, $gaLabel);

    //Check mysql timeout
    if (false === $conn->ping()) {
        $conn->close();
        $conn->connect();
    }

    if ($isSuccess) {
        $logger->logMtSuccess('Send daily MT (1 SMS /day) for telco DTAC success', $logData);
    } else {
        $logger->logMtError('Send daily MT (1 SMS /day) for telco DTAC error!', $logData, true);
    }

    //Insert MT
    $status = $isSuccess ? MT::MT_STATUS_SUCCESS : MT::MT_STATUS_FAIL;
    try {
        //Get count phones of telco DTAC, keyword
        $sql = 'SELECT COUNT(phone) FROM user WHERE telco_id = :telcoDTAC AND keyword = :keyword';
        $stmt = $conn->executeQuery($sql, ['telcoDTAC' => Telco::DTAC, 'keyword' => $keyword]);
        $phoneCount = intval($stmt->fetchColumn(0));

        insertMTDTAC($conn, $phone, $shortCode, $telcoId, $text, $keyword, MT::MT_TYPE_DAILY, $status, $phoneCount, $result);

        //Update `last_mt_at` & `last_link_id` for each phone
        $conn->update(
            'user',
            [
                'last_mt_at' => DateTime::getLocalTime(),
                'last_mt_at_utc' => DateTime::getUTCTime(),
                'last_link_id' => $newLinkId,
                'last_mt_response' => $result,
            ],
            [
                'telco_id' => Telco::DTAC,
                'keyword' => $keyword,
            ]
        );

    } catch (\Exception $ex) {
        $logger->logMtError('Insert a record into table `mt` fail, because '.$ex->getMessage(), $logData, true);
    }

    return $phoneCount;
}

return $console;

