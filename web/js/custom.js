$(document).ready(function () {
    $('#form-link').on('submit', function () {
        if ($('#keyword').val() == '') {
            alert('Bạn phải chọn 1 keyword!');
            $('#keyword').focus();

            return false;
        }

        if ($('#link').val().trim() == '') {
            alert('Link không được trống!');
            $('#link').val('');
            $('#link').focus();

            return false;
        }
    });

    var pathArray = window.location.pathname.split('/');
    var basePath = '';
    if ('index_dev.php' == pathArray[1]) {
        basePath = '/index_dev.php';
    }

    $('#select_keyword').on('change', function () {
        var count = $('#select_count').val();
        window.location.href = basePath + '/links?keyword=' + $(this).val() + '&limit=' + count;
    });

    $('#select_count').on('change', function () {
        var keyword = $('#select_keyword').val();
        var count = $(this).val();

        window.location.href = basePath + '/links?keyword=' + keyword.toUpperCase() + '&limit=' + count;
    });

    var queryParams = getUrlVars();
    if (typeof queryParams['keyword'] !== 'undefined') {
        var keyword = queryParams['keyword'];
        $('select[name="select_keyword"] option[value="' + keyword.toUpperCase() + '"]').prop('selected', 'selected');
    }

    if (typeof queryParams['limit'] !== 'undefined') {
        var limit = queryParams['limit'];
        $('select[name="select_count"] option[value="' + limit + '"]').prop('selected', 'selected');
    }

    //Show hyperlink content
    $('.link-content').each(function () {
        var text = $(this).text().trim();
        if ('http' == text.substr(0, 4).toLowerCase()) {
            $(this).html('<a target="_blank" href="' + text + '">' + text + '</a>');
        }
    });

    //Handle show main keyword
    if ('/links/add' == window.location.pathname) {
        if (typeof queryParams['keyword'] !== 'undefined') {
            var keyword = queryParams['keyword'];
            $('select[name="keyword"] option[value="' + keyword.toUpperCase() + '"]').prop('selected', 'selected');
            $('#link').focus();
        }
    }

});

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }

    return vars;
}
